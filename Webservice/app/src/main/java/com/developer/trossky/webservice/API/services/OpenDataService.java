package com.developer.trossky.webservice.API.services;


import com.developer.trossky.webservice.API.model.SuperData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenDataService {


    @GET("pk3c-3zb4.json")
    Call<List<SuperData>> getSuperData();


    @GET("pk3c-3zb4.json?concepto=FUNCIONAMIENTO")
    Call<List<SuperData>> getSuperDataBy(/*@Query("concepto") String concepto*/);

    @GET("pk3c-3zb4.json?concepto=FUNCIONAMIENTO")
    Call<List<SuperData>> getSuperDataByFuncionamiento();

    @GET("pk3c-3zb4.json?concepto=TOTAL FUNCIONAMIENTO")
    Call<List<SuperData>> getSuperDataByTotalFuncionamiento();

    @GET("pk3c-3zb4.json?concepto=GASTOS DE PERSONAL")
    Call<List<SuperData>> getSuperDataByGastosDePersonal();


    @GET("pk3c-3zb4.json?concepto=GASTOS GENERALES")
    Call<List<SuperData>> getSuperDataByGastosGenerales();

    @GET("pk3c-3zb4.json?concepto=TOTAL GASTOS GENERALES")
    Call<List<SuperData>> getSuperDataByTotalGastosGastosGenerales();

    @GET("pk3c-3zb4.json?concepto=TRANSFERENCIAS CORRIENTES")
    Call<List<SuperData>> getSuperDataByTransferenciasCorrientes();


    @GET("pk3c-3zb4.json?concepto=TOTAL TRANSFERENCIAS")
    Call<List<SuperData>> getSuperDataByTotalTransferenciasCorrientes();

    @GET("pk3c-3zb4.json?concepto=COMPRA DE BIENES Y SERVICIOS")
    Call<List<SuperData>> getSuperDataByCompraDeBienesYServicios();

    @GET("pk3c-3zb4.json?concepto=TOTAL COMPRA DE BIENES Y SERVICIOS")
    Call<List<SuperData>> getSuperDataByTotalCompraDeBienesYServicios();

    @GET("pk3c-3zb4.json?concepto=INVERSION")
    Call<List<SuperData>> getSuperDataByInversion();

    @GET("pk3c-3zb4.json?concepto=IMPLEMENTACIÓN PROGRAMA DE GESTIÓN DOCUMENTAL NACIONAL")
    Call<List<SuperData>> getSuperDataByImplementacionProgramaGestionDocumetal();
}
