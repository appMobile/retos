package com.developer.trossky.webservice;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.trossky.webservice.API.model.SuperData;
import com.developer.trossky.webservice.API.services.OpenDataService;
import com.developer.trossky.webservice.Businesses.DataBusiness;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.developer.trossky.webservice.constant.K.HTTP_BAD;
import static com.developer.trossky.webservice.constant.K.HTTP_SERVER;
import static com.developer.trossky.webservice.constant.K.URL_API;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_OK;


public class SuperDataFragment extends Fragment {

    List<SuperData> addStudentList= new ArrayList<>();
    private DataBusiness dataBusiness;

    public  ContentAdapter adapter;





    Retrofit retrofit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        retrofit = new Retrofit.Builder()
                .baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.view_data, container, false);
        SuperData(getContext());


        adapter = new ContentAdapter(getAddStudentList());

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return recyclerView;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView avator;
        public TextView name;
        public TextView description;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_super_data, parent, false));

            avator = (ImageView) itemView.findViewById(R.id.list_avatar);
            name = (TextView) itemView.findViewById(R.id.list_title);
            description = (TextView) itemView.findViewById(R.id.list_desc);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DataDetailsActivity.class);
                    System.out.println();
                    intent.putExtra(DataDetailsActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });

        }
    }

    public void SuperData(final Context context) {


        OpenDataService openDataService = retrofit.create(OpenDataService.class);
        Call<List<SuperData>> superData = openDataService.getSuperData();

        superData.enqueue(new Callback<List<SuperData>>() {
            @Override
            public void onResponse(Call<List<SuperData>> call, Response<List<SuperData>> response) {

                switch (response.code()) {
                    case HTTP_OK:
                      response.body();

                        adapter.addStudentList(response.body());


                        Toast.makeText(context.getApplicationContext(), R.string.ok, Toast.LENGTH_LONG).show();

                        break;
                    case HTTP_BAD:
                        Toast.makeText(context.getApplicationContext(), R.string.bad, Toast.LENGTH_LONG).show();
                        break;

                    case HTTP_CONFLICT:
                        Toast.makeText(context.getApplicationContext(), R.string.conflict, Toast.LENGTH_LONG).show();
                        break;
                    case HTTP_SERVER:
                        Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();

                        break;
                    default:
                        Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();
                        break;
                }


            }

            @Override
            public void onFailure(Call<List<SuperData>> call, Throwable t) {
                Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }

    /**
     * Adapter to display recycler view.
     */
    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {


        public List<SuperData> students;


        public ContentAdapter(List<SuperData> students) {
            this.students = students;

        }

        public  void addStudentList(List<SuperData> studentList) {
            students=studentList;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            SuperData student=students.get(position);
            holder.avator.setImageResource(R.drawable.lupa);
            holder.name.setText(student.getConcepto());
            holder.description.setText("RECURSO: "+student.getRecurso()+"\n"+
            " APROPIACIÓN VIEGENTE :"+student.getApropiaci_n_vigente()+ " CDP ->"+student.getCdp());
        }

        @Override
        public int getItemCount() {
            return students.size();
        }
    }
    public List<SuperData> getAddStudentList() {
        return addStudentList;
    }

    public void setAddStudentList(List<SuperData> addStudentList) {
        this.addStudentList = addStudentList;
    }



}
