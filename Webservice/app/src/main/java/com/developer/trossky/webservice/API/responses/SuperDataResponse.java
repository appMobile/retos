package com.developer.trossky.webservice.API.responses;



import com.developer.trossky.webservice.API.model.SuperData;


import java.util.List;

/**
 * Created by luis on 8/10/16.
 */

public class SuperDataResponse {

    List<SuperData> consults;

    public List<SuperData> getConsults() {
        return consults;
    }

    public void setConsults(List<SuperData> consults) {
        this.consults = consults;
    }
}
