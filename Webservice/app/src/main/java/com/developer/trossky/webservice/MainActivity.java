package com.developer.trossky.webservice;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.developer.trossky.webservice.API.model.SuperData;
import com.developer.trossky.webservice.Businesses.DataBusiness;

import java.sql.SQLOutput;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.developer.trossky.webservice.constant.K.URL_API;

public class MainActivity extends AppCompatActivity {

  private DataBusiness dataBusiness;
  public List<SuperData> students;
    FragmentManager fm1 = getSupportFragmentManager();

    Fragment fragment1;

    private Retrofit retrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBusiness= new DataBusiness();


        fm1 = getSupportFragmentManager();
        fragment1 = fm1.findFragmentById(R.id.activity_main);

        //dataBusiness.SuperData(getApplicationContext());
        if (fragment1 == null) {
            fragment1 = new SuperDataFragment();
            fm1.beginTransaction()
                    .add(R.id.activity_main, fragment1)
                    .commit();


        }

        retrofit = new Retrofit.Builder()
                .baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        } else if (id == R.id.route_filter){
            System.out.println("HOLAAAAAAAAAA");

            createDialogAbout();

            Toast.makeText(this, "No funciona " ,Toast.LENGTH_SHORT);

        }
        return super.onOptionsItemSelected(item);
    }

    private void createDialogAbout() {
        int selected;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        final CharSequence [] levels ={
                getResources().getString(R.string.difficulty_easy),
                getResources().getString(R.string.difficulty_harder),
                getResources().getString(R.string.difficulty_expert),
                "GASTOS GENERALES",
                "TOTAL GASTOS GENERALES",
                "TRANSFERENCIAS CORRIENTES",
                "TOTAL TRANSFERENCIAS",
                "COMPRA DE BIENES Y SERVICIOS",
                "TOTAL COMPRA DE BIENES Y SERVICIOS",
                "INVERSION",
                "IMPLEMENTACIÓN PROGRAMA DE GESTIÓN DOCUMENTAL NACIONAL",
                "TODOS"
        };

        selected=0;
        builder.setSingleChoiceItems(levels, selected,

                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item){
                        dialog.dismiss();
                        dataBusiness.SuperData(getApplicationContext(),retrofit,((SuperDataFragment)fragment1).adapter,item);

                    }


                });

        builder.create().show();
    }


}
