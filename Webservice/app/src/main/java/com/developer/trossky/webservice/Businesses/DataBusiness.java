package com.developer.trossky.webservice.Businesses;


import android.content.Context;

import android.widget.Toast;

import com.developer.trossky.webservice.API.model.SuperData;
import com.developer.trossky.webservice.API.services.OpenDataService;
import com.developer.trossky.webservice.R;
import com.developer.trossky.webservice.SuperDataFragment;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.developer.trossky.webservice.constant.K.HTTP_BAD;
import static com.developer.trossky.webservice.constant.K.HTTP_SERVER;
import static com.developer.trossky.webservice.constant.K.URL_API;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by luis on 15/10/16.
 */

public class DataBusiness {


    private List<SuperData> superDatas;
    public SuperDataFragment.ContentAdapter contentAdapter;

    public List<SuperData> getSuperDatas() {
        return superDatas;
    }

    public void setSuperDatas(List<SuperData> superDatas) {
        this.superDatas = superDatas;
    }

    public void SuperData(final Context context, Retrofit retrofit, final SuperDataFragment.ContentAdapter adapter, int option) {



        OpenDataService openDataService = retrofit.create(OpenDataService.class);
        Call<List<SuperData>> superData= superData = openDataService.getSuperDataByFuncionamiento(/*"FUNCIONAMIENTO"*/);
        switch (option){
            case 0:
                 superData = openDataService.getSuperDataByFuncionamiento(/*"FUNCIONAMIENTO"*/);
                break;
            case 1:
                superData = openDataService.getSuperDataByTotalFuncionamiento();
                break;
            case 2:
                superData = openDataService.getSuperDataByGastosDePersonal();
                break;
            case 3:
                superData = openDataService.getSuperDataByGastosGenerales();
                break;
            case 4:
                superData = openDataService.getSuperDataByTotalGastosGastosGenerales();
                break;
            case 5:
                superData = openDataService.getSuperDataByTransferenciasCorrientes();
                break;
            case 6:
                superData = openDataService.getSuperDataByTotalTransferenciasCorrientes();
                break;
            case 7:
                superData = openDataService.getSuperDataByCompraDeBienesYServicios();
                break;
            case 8:
                superData = openDataService.getSuperDataByTotalCompraDeBienesYServicios();
                break;
            case 9:
                superData = openDataService.getSuperDataByInversion();
                break;
            case 10:
                superData = openDataService.getSuperDataByImplementacionProgramaGestionDocumetal();
                break;
            default:
                superData = openDataService.getSuperData();
                break;
        }


        superData.enqueue(new Callback<List<SuperData>>() {
            @Override
            public void onResponse(Call<List<SuperData>> call, Response<List<SuperData>> response) {

                switch (response.code()) {
                    case HTTP_OK:



                        adapter.addStudentList(response.body());

                        Toast.makeText(context.getApplicationContext(), R.string.ok, Toast.LENGTH_LONG).show();

                        break;
                    case HTTP_BAD:
                        Toast.makeText(context.getApplicationContext(), R.string.bad, Toast.LENGTH_LONG).show();
                        break;

                    case HTTP_CONFLICT:
                        Toast.makeText(context.getApplicationContext(), R.string.conflict, Toast.LENGTH_LONG).show();
                        break;
                    case HTTP_SERVER:
                        Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();

                        break;
                    default:
                        Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();
                        break;
                }


            }

            @Override
            public void onFailure(Call<List<SuperData>> call, Throwable t) {
                Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }


}
