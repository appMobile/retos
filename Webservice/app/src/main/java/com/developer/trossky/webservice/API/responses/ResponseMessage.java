package com.developer.trossky.webservice.API.responses;

/**
 * Created by luis on 22/11/16.
 */

public class ResponseMessage {
    private String message;

    public ResponseMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
