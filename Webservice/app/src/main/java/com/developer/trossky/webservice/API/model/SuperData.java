package com.developer.trossky.webservice.API.model;

/**
 * Created by luis on 15/10/16.
 */

public class SuperData {
    private Double apropiaci_n_disponible;
    private Double apropiaci_n_vigente;
    private Double cdp;
    private Double compromisos;
    private Double compromisos_compromisos_apro_vigente;
    private String  concepto;
    private Double cuentas_por_pagar;
    private String  fuente;
    private Double obligaciones;
    private Double obligaciones_obligaciones_apro_vigente;
    private Double pagos;
    private Double pagos_pagos_apro_vigente;
    private String  rec;
    private String  recurso;
    private Double reserva_presupuestal;
    private String  sit;

    public SuperData(Double apropiaci_n_disponible, Double apropiaci_n_vigente, Double cdp, Double compromisos, Double compromisos_compromisos_apro_vigente, String concepto, Double cuentas_por_pagar, String fuente, Double obligaciones, Double obligaciones_obligaciones_apro_vigente, Double pagos, Double pagos_pagos_apro_vigente, String rec, String recurso, Double reserva_presupuestal, String sit) {
        this.apropiaci_n_disponible = apropiaci_n_disponible;
        this.apropiaci_n_vigente = apropiaci_n_vigente;
        this.cdp = cdp;
        this.compromisos = compromisos;
        this.compromisos_compromisos_apro_vigente = compromisos_compromisos_apro_vigente;
        this.concepto = concepto;
        this.cuentas_por_pagar = cuentas_por_pagar;
        this.fuente = fuente;
        this.obligaciones = obligaciones;
        this.obligaciones_obligaciones_apro_vigente = obligaciones_obligaciones_apro_vigente;
        this.pagos = pagos;
        this.pagos_pagos_apro_vigente = pagos_pagos_apro_vigente;
        this.rec = rec;
        this.recurso = recurso;
        this.reserva_presupuestal = reserva_presupuestal;
        this.sit = sit;
    }

    public Double getApropiaci_n_disponible() {
        return apropiaci_n_disponible;
    }

    public void setApropiaci_n_disponible(Double apropiaci_n_disponible) {
        this.apropiaci_n_disponible = apropiaci_n_disponible;
    }

    public Double getApropiaci_n_vigente() {
        return apropiaci_n_vigente;
    }

    public void setApropiaci_n_vigente(Double apropiaci_n_vigente) {
        this.apropiaci_n_vigente = apropiaci_n_vigente;
    }

    public Double getCdp() {
        return cdp;
    }

    public void setCdp(Double cdp) {
        this.cdp = cdp;
    }

    public Double getCompromisos() {
        return compromisos;
    }

    public void setCompromisos(Double compromisos) {
        this.compromisos = compromisos;
    }

    public Double getCompromisos_compromisos_apro_vigente() {
        return compromisos_compromisos_apro_vigente;
    }

    public void setCompromisos_compromisos_apro_vigente(Double compromisos_compromisos_apro_vigente) {
        this.compromisos_compromisos_apro_vigente = compromisos_compromisos_apro_vigente;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Double getCuentas_por_pagar() {
        return cuentas_por_pagar;
    }

    public void setCuentas_por_pagar(Double cuentas_por_pagar) {
        this.cuentas_por_pagar = cuentas_por_pagar;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Double getObligaciones() {
        return obligaciones;
    }

    public void setObligaciones(Double obligaciones) {
        this.obligaciones = obligaciones;
    }

    public Double getObligaciones_obligaciones_apro_vigente() {
        return obligaciones_obligaciones_apro_vigente;
    }

    public void setObligaciones_obligaciones_apro_vigente(Double obligaciones_obligaciones_apro_vigente) {
        this.obligaciones_obligaciones_apro_vigente = obligaciones_obligaciones_apro_vigente;
    }

    public Double getPagos() {
        return pagos;
    }

    public void setPagos(Double pagos) {
        this.pagos = pagos;
    }

    public Double getPagos_pagos_apro_vigente() {
        return pagos_pagos_apro_vigente;
    }

    public void setPagos_pagos_apro_vigente(Double pagos_pagos_apro_vigente) {
        this.pagos_pagos_apro_vigente = pagos_pagos_apro_vigente;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public Double getReserva_presupuestal() {
        return reserva_presupuestal;
    }

    public void setReserva_presupuestal(Double reserva_presupuestal) {
        this.reserva_presupuestal = reserva_presupuestal;
    }

    public String getSit() {
        return sit;
    }

    public void setSit(String sit) {
        this.sit = sit;
    }
}
