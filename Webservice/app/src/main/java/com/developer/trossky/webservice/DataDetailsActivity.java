package com.developer.trossky.webservice;

import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.trossky.webservice.API.model.SuperData;
import com.developer.trossky.webservice.API.services.OpenDataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.developer.trossky.webservice.constant.K.HTTP_BAD;
import static com.developer.trossky.webservice.constant.K.HTTP_SERVER;
import static com.developer.trossky.webservice.constant.K.URL_API;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_OK;

public class DataDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_POSITION = "position";

    Retrofit retrofit;
    SuperData student;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_details);

        retrofit = new Retrofit.Builder()
                .baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SuperData(retrofit);


    }


    public void SuperData( Retrofit retrofit) {



        OpenDataService openDataService = retrofit.create(OpenDataService.class);
        Call<List<SuperData>> superData = openDataService.getSuperData();

        superData.enqueue(new Callback<List<SuperData>>() {
            @Override
            public void onResponse(Call<List<SuperData>> call, Response<List<SuperData>> response) {

                switch (response.code()) {
                    case HTTP_OK:
                        loadProfile(response.body());

                        //Toast.makeText(context.getApplicationContext(), R.string.ok, Toast.LENGTH_LONG).show();
                        break;
                    case HTTP_BAD:
                        //Toast.makeText(context.getApplicationContext(), R.string.bad, Toast.LENGTH_LONG).show();
                        break;

                    case HTTP_CONFLICT:
                        //Toast.makeText(context.getApplicationContext(), R.string.conflict, Toast.LENGTH_LONG).show();
                        break;
                    case HTTP_SERVER:
                        //Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();

                        break;
                    default:
                        //Toast.makeText(context.getApplicationContext(), R.string.error_server, Toast.LENGTH_LONG).show();
                        break;
                }


            }

            @Override
            public void onFailure(Call<List<SuperData>> call, Throwable t) {
                //Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }

    private void loadProfile(List<SuperData> student_list) {

        // Set Collapsing Toolbar layout to the screen
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        // Set title of Detail page
        // collapsingToolbar.setTitle(getString(R.string.item_title));

        int postion = getIntent().getIntExtra(EXTRA_POSITION, 0);
        student = student_list.get(postion);
        String places = student.getConcepto();
        collapsingToolbar.setTitle(places);

        String placeDetails = ("RECURSO: "+student.getRecurso()+"\n"+
                " APROPIACIÓN VIEGENTE :"+student.getApropiaci_n_vigente()+ " CDP ->"+student.getCdp());
        TextView placeDetail = (TextView) findViewById(R.id.place_detail);
        placeDetail.setText(placeDetails);

        String placeLocations = ("FUENTE : "+student.getFuente());
        TextView placeLocation =  (TextView) findViewById(R.id.place_location);
        placeLocation.setText(placeLocations);


        ImageView placePicutre = (ImageView) findViewById(R.id.image);
        placePicutre.setImageResource(R.drawable.lupa);
    };
}
