package app.trossky.com.reto3.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.sql.SQLOutput;

import app.trossky.com.reto3.R;
import app.trossky.com.reto3.businesses.Points_GameBusiness;
import app.trossky.com.reto3.businesses.TicTacToeBusiness;
import app.trossky.com.reto3.constants.K;

public class TicTacToeActivity extends AppCompatActivity {


    // Represents the internal state of the game
    private TicTacToeBusiness ticTacToeBusiness;
    private Points_GameBusiness points_gameBusiness;

    private Button newGameBtn;
    private Button mBoardButtons[];
    TextView name_player;
    TextView result_table_TextView;
    private FrameLayout frameLayoutNewGame;
    private  String result_table;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        mBoardButtons = new Button[K.BOARD_SIZE];

        mBoardButtons[0] = ((Button) findViewById(R.id.b1));
        mBoardButtons[1] = ((Button) findViewById(R.id.b2));
        mBoardButtons[2] = ((Button) findViewById(R.id.b3));
        mBoardButtons[3] = ((Button) findViewById(R.id.b4));
        mBoardButtons[4] = ((Button) findViewById(R.id.b5));
        mBoardButtons[5] = ((Button) findViewById(R.id.b6));
        mBoardButtons[6] = ((Button) findViewById(R.id.b7));
        mBoardButtons[7] = ((Button) findViewById(R.id.b8));
        mBoardButtons[8] = ((Button) findViewById(R.id.b9));
        frameLayoutNewGame = (FrameLayout) findViewById(R.id.frameNnewGame);
        newGameBtn = (Button) findViewById(R.id.newGame);
        name_player = (TextView) findViewById(R.id.name_player);
        result_table_TextView=(TextView) findViewById(R.id.result_table);

        ticTacToeBusiness = new TicTacToeBusiness();


        //TODO Loading  point's players of database. Now, by default is 0,0,0
        points_gameBusiness = new Points_GameBusiness(0, 0, 0);




        startNewGame();
        getResult_Table();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add("New Game");
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startNewGame();
        return true;
    }


    // Set up the game board.
    private void startNewGame() {
        ticTacToeBusiness.clearBoard();
        for (int i = 0; i < mBoardButtons.length; i++) {
            mBoardButtons[i].setText(" ");
            mBoardButtons[i].setEnabled(true);
            mBoardButtons[i].setOnClickListener(new ButtonClickListener(i));
        }

        name_player.setText( getAuth_Username()+", "+getString(R.string.first_human));
        frameLayoutNewGame.setVisibility(View.GONE);



    }

    private void setMove(char player, int location) {
        ticTacToeBusiness.setMove(player, location + 1);
        mBoardButtons[location].setEnabled(false);
        mBoardButtons[location].setText(String.valueOf(player));
        if (player == K.HUMAN_PLAYER_1)
            mBoardButtons[location].setTextColor(K.COLOR_HUMAN_PLAYER);
        else
            mBoardButtons[location].setTextColor(K.COLOR_COMPUTER_PLAYER);
    }
    public void getResult_Table(){
        result_table=getAuth_Username()+":"+ points_gameBusiness.getCount_human()+" "+
                getString(R.string.counterTie)+":"+points_gameBusiness.getCount_tie()+" "+
                getString(R.string.counterAndroid)+":"+points_gameBusiness.getCount_android();

        System.out.println(result_table);
        result_table_TextView.setText(result_table);
    }

    public void disabledBoard() {
        System.out.println("New" +
                "");
        getResult_Table();



        for (int i = 0; i < mBoardButtons.length; i++) {
            mBoardButtons[i].setEnabled(false);
        }
        frameLayoutNewGame.setVisibility(View.VISIBLE);
    }

    public String getAuth_Username() {
             /* Data user auth*/
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (!extras.isEmpty() || extras != null) {
            return (extras.getString("USERNAME"));

        } else {

            return "Invitado";
        }


    }



    private class ButtonClickListener implements View.OnClickListener {

        int location;


        public ButtonClickListener(int location) {

            this.location = location;
            newGameBtn.setOnClickListener(this);
        }

        public void onClick(View view) {

            if (mBoardButtons[location].isEnabled()) {
                setMove(K.HUMAN_PLAYER_1, location);
                // If no winner yet, let the computer make a move
                int winner = ticTacToeBusiness.checkForWinner();
                if (winner == 0) {
                    name_player.setText(R.string.turn_computer);
                    int move = ticTacToeBusiness.getComputerMove();
                    setMove(K.COMPUTER_PLAYER, move);
                    winner = ticTacToeBusiness.checkForWinner();
                }

                if (winner == 0)
                    name_player.setText(getAuth_Username() + ", " + getString(R.string.turn_human));
                else if (winner == 1) {
                    name_player.setText(R.string.result_tie);
                    points_gameBusiness.sum_points(K.TIE, 1);

                    disabledBoard();

                } else if (winner == 2) {
                    name_player.setText(getAuth_Username() + ", " + getString(R.string.result_human_wins));
                    points_gameBusiness.sum_points(K.HUMAN_PLAYER_1, 1);
                    disabledBoard();
                } else {
                    name_player.setText(R.string.result_computer_wins);
                    points_gameBusiness.sum_points(K.COMPUTER_PLAYER, 1);
                    disabledBoard();

                }

            }
            if (view.getId() == R.id.newGame) {
                startNewGame();
            }


        }

    }
}
