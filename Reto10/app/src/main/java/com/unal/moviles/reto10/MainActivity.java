package com.unal.moviles.reto10;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Marker me;
    private static GoogleApiClient mGoogleApiClient;
    private View root;
    private Location currentLocation;
    private LocationRequest request;
    private BitmapDescriptor markerIcon;
    private CheckBox bupa;
    private CheckBox battery;
    private CheckBox medicine;
    private CheckBox bulbs;
    private static List<RecycleSite> sites;
    private static List<Marker> markers;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = View.inflate(this,R.layout.activity_main,null);
        sites = new ArrayList<RecycleSite>();
        markers = new ArrayList<Marker>();
        bupa = (CheckBox) root.findViewById(R.id.bupa);
        bupa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new RecycleSites().execute(buildUrl());
            }
        });
        battery = (CheckBox) root.findViewById(R.id.battery);
        battery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new RecycleSites().execute(buildUrl());
            }
        });
        medicine = (CheckBox) root.findViewById(R.id.medicine);
        medicine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new RecycleSites().execute(buildUrl());
            }
        });
        bulbs = (CheckBox) root.findViewById(R.id.bulbs);
        bulbs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new RecycleSites().execute(buildUrl());
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        buildGoogleClient();
        setContentView(root);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buildGoogleClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void startUpdates(){
        request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setFastestInterval(10000);
        request.setInterval(20000);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,request,this);

    }
    public String buildUrl(){
        boolean orCategory = false;
        StringBuilder url = new StringBuilder("http://servicedatosabiertoscolombia.cloudapp.net/v1/Ministerio_de_Ambiente/puntosposconsumo");
        url.append("?$top=150");

        if(bupa.isChecked()){
            url.append("&$filter=%22categoria%22=%27BUPA%27");
            orCategory = true;
        }
        if(battery.isChecked()){
            if(!orCategory){
                url.append("&$filter=%22categoria%22=%27PILAS%27");
                orCategory = true;
            }else{
                url.append("or%22categoria%22=%27PILAS%27");
            }
        }
        if(medicine.isChecked()){
            if(!orCategory){
                url.append("&$filter=%22categoria%22=%27MEDICAMENTOS%27");
                orCategory = true;
            }else{
                url.append("or%22categoria%22=%27MEDICAMENTOS%27");
            }
        }
        if (bulbs.isChecked()){
            if(!orCategory){
                url.append("&$filter=%22categoria%22=%27BOMBILLAS%27");
                orCategory = true;
            }else{
                url.append("or%22categoria%22=%27BOMBILLAS%27");
            }
        }


        if(!orCategory){ // There are no check buttons selected
            url.append("&$filter=%22categoria%22=%27PILAS%27or%22categoria%22=%27MEDICAMENTOS%27or%22categoria%22=%27BUPA%27or" +
                    "%22categoria%22=%27BOMBILLAS%27");
        }
        url.append("&$format=json");
        //Log.e("algo",url.toString());
        return url.toString();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("bupa",bupa.isChecked());
        outState.putBoolean("battery",battery.isChecked());
        outState.putBoolean("medicine",medicine.isChecked());
        outState.putBoolean("bulbs",bulbs.isChecked());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        bupa.setChecked(savedInstanceState.getBoolean("bupa"));
        battery.setChecked(savedInstanceState.getBoolean("battery"));
        medicine.setChecked(savedInstanceState.getBoolean("medicine"));
        bulbs.setChecked(savedInstanceState.getBoolean("bulbs"));

    }

    @Override
    public void onConnected(Bundle bundle) {
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LatLng myLocation;
        if(me!=null){
            me.remove();
        }
        if(currentLocation!=null){
            Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.user);
            Bitmap scaled = Bitmap.createScaledBitmap(image,70,70,true);
            markerIcon = BitmapDescriptorFactory.fromBitmap(scaled);
            myLocation = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            me = map.addMarker(new MarkerOptions()
                    .position(myLocation)
                    .title("me")
                    .icon(markerIcon));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(myLocation)
                    .zoom(16)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
        new RecycleSites().execute(buildUrl());
        startUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        LatLng myLocation;
        if(me!=null){
            me.remove();
        }
        if(currentLocation!=null){
            myLocation = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
            if(markerIcon == null){
                Bitmap image = BitmapFactory.decodeResource(getResources(),R.drawable.user);
                Bitmap scaled = Bitmap.createScaledBitmap(image,80,80,true);
                markerIcon = BitmapDescriptorFactory.fromBitmap(scaled);

            }
            me = map.addMarker(new MarkerOptions()
                .position(myLocation)
                .title("me")
                .icon(markerIcon));
        }
        new RecycleSites().execute(buildUrl());

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class RecycleSites extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... strings) {
            String urlString = strings[0];
            StringBuilder places = null;
            HttpURLConnection urlConnection=null;

            try {
                URL url = new URL(urlString);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                places = new StringBuilder();
                String line = "";
                while ((line=br.readLine())!=null){
                    places.append(line);
                }
                JSONObject object = new JSONObject(places.toString());
                JSONArray results = object.getJSONArray("d");
                sites.clear();
                for(int i = 0; i<results.length(); i++){

                    String name = results.getJSONObject(i).getString("vocero");
                    String email = results.getJSONObject(i).getString("email");
                    String category = results.getJSONObject(i).getString("categoria");
                    String address = results.getJSONObject(i).getString("direccion");
                    double lat = results.getJSONObject(i).getDouble("latitud");
                    String lngError = results.getJSONObject(i).getString("longitud");
                    double lng;
                    if(lngError.charAt(0)=='-' && lngError.charAt(1)=='-'){
                        lng = Double.parseDouble(lngError.substring(1));
                    }else{
                        lng = Double.parseDouble(lngError);
                    }


                    sites.add(new RecycleSite(getApplicationContext(),address,category,email,lat,lng,name));
                }
            }catch (Exception e){
                Log.e("com.unal.moviles.reto10", e.toString());
            }finally {
                urlConnection.disconnect();
            }
            return null;
        }



        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(markers!=null){
                for(Marker marker:markers){
                    marker.remove();
                }
            }
            List<RecycleSite> copy = new ArrayList<>(sites);
            for(RecycleSite i : copy){
                markers.add(map.addMarker(new MarkerOptions()
                    .position(new LatLng(i.getLat(), i.getLng()))
                    .icon(i.getIcon())
                    .title(i.getName())
                    .snippet(i.getAddress())));
            }
        }
    }
}
