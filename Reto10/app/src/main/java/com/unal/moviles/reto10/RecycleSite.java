package com.unal.moviles.reto10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

/**
 * Created by andresgutierrez on 11/9/15.
 */
public class RecycleSite {
    private Context context;
    private String category;
    private String name;
    private String email;
    private String address;
    private double lat;
    private double lng;

    private BitmapDescriptor icon;

    public RecycleSite(Context context, String address, String category, String email, double lat, double lng, String name) {
        this.address = address;
        this.context = context;
        this.email = email;
        this.category = category;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.icon = getIcon();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BitmapDescriptor getIcon() {
        BitmapDescriptor icon;
        Bitmap image = null;
        Bitmap scaled = null;
        switch (category){
            case "BUPA":
                image = BitmapFactory.decodeResource(context.getResources(),R.drawable.motorcycle);
                break;
            case "MEDICAMENTOS":
                image = BitmapFactory.decodeResource(context.getResources(),R.drawable.medicine);

                break;
            case "BOMBILLAS":
                image = BitmapFactory.decodeResource(context.getResources(),R.drawable.bulb);

                break;
            case "PILAS":
                image = BitmapFactory.decodeResource(context.getResources(),R.drawable.battery);

                break;
        }
        scaled = Bitmap.createScaledBitmap(image,80,80,true);
        icon = BitmapDescriptorFactory.fromBitmap(scaled);
        return icon;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
