package com.trossky.developer.business;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.trossky.developer.database.R;
import com.trossky.developer.database.model.Business;

public class BusinessActivity extends AppCompatActivity {

    public static final String EXTRA_LAWYER_ID = "extra_lawyer_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        BusinessFragment fragment = (BusinessFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_business);

        if (fragment == null) {
            fragment = BusinessFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_business, fragment)
                    .commit();
        }



    }

}
