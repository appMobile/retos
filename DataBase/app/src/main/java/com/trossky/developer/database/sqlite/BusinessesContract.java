package com.trossky.developer.database.sqlite;

import android.content.ContentValues;
import android.provider.BaseColumns;

import java.util.UUID;




public class BusinessesContract {

    interface ColumnasEmpresa {
        String ID = "id";
        String NOMBRE_EMPRESA = "nombre_empresa";
        String URL_EMPRESA = "url_empresa";
        String TELEFONO = "telefono";
        String EMAIL = "email";
        String PRODUCTO_SERVICIO = "producto_servicio";
        String CLASIFICACION = "clasificacion";
    }

    public static  abstract class BusinessEntry implements BaseColumns {
        public static final String TABLE_NAME = "business";

        public static final String ID = "id";
        public static final String NANE = "name";
        public static final String URL = "url";
        public static final String TELEPHONE = "telephone";
        public static final String EMAIL = "email";
        public static final String PRODUCT_SERVICE = "product_service";
        public static final String CLASSIFICATION = "classification";



    }

    private BusinessesContract() {
    }


}
