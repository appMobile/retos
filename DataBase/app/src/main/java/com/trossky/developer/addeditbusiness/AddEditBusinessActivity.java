package com.trossky.developer.addeditbusiness;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.trossky.developer.business.BusinessActivity;
import com.trossky.developer.database.R;

public class AddEditBusinessActivity extends AppCompatActivity {
    public static final int REQUEST_ADD_LAWYER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_business);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String lawyerId = getIntent().getStringExtra(BusinessActivity.EXTRA_LAWYER_ID);

        setTitle(lawyerId == null ? "Añadir Empresa" : "Editar Empresa");

        AddEditBusinessFragment addEditBusinessFragment= (AddEditBusinessFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_add_edit_business);
        if (addEditBusinessFragment == null) {
            addEditBusinessFragment = AddEditBusinessFragment.newInstance(lawyerId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_add_edit_business, addEditBusinessFragment)
                    .commit();
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
