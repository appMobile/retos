package com.trossky.developer.businessdetail;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.trossky.developer.addeditbusiness.AddEditBusinessActivity;
import com.trossky.developer.business.BusinessActivity;
import com.trossky.developer.business.BusinessFragment;
import com.trossky.developer.database.R;
import com.trossky.developer.database.model.Business;
import com.trossky.developer.database.sqlite.LawyersDbHelper;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BusinessDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusinessDetailFragment extends Fragment {
    private static final String ARG_LAWYER_ID = "lawyerId";

    private String mLawyerId;

    private CollapsingToolbarLayout mCollapsingView;
    private ImageView mAvatar;
    private TextView mPhoneNumber;
    private TextView mSpecialty;
    private TextView mBio;
    private TextView mUrl;
    private TextView mMail;

    private LawyersDbHelper mLawyersDbHelper;


    public BusinessDetailFragment() {
        // Required empty public constructor
    }

    public static BusinessDetailFragment newInstance(String lawyerId) {
        BusinessDetailFragment fragment = new BusinessDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LAWYER_ID, lawyerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mLawyerId = getArguments().getString(ARG_LAWYER_ID);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_business_detail, container, false);
        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        mAvatar = (ImageView) getActivity().findViewById(R.id.iv_avatar);
        mPhoneNumber = (TextView) root.findViewById(R.id.tv_phone_number);
        mSpecialty = (TextView) root.findViewById(R.id.tv_specialty);
        mBio = (TextView) root.findViewById(R.id.tv_bio);
        mUrl = (TextView) root.findViewById(R.id.tv_url);
        mMail = (TextView) root.findViewById(R.id.tv_mail);

        mLawyersDbHelper = new LawyersDbHelper(getActivity());

        loadLawyer();

        return root;
    }

    private void loadLawyer() {
        new GetLawyerByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                createDialogExit();

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void createDialogExit() {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.quit_question)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    public void onClick(DialogInterface  dialog, int id){
                        new DeleteLawyerTask().execute();

                    }
                })
                .setNegativeButton(R.string.no,null);
        builder.setCancelable(false);

        builder.create().show();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode ==BusinessFragment.REQUEST_UPDATE_DELETE_LAWYER) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showLawyer(Business lawyer) {
        mCollapsingView.setTitle(lawyer.getName());
        Glide.with(this)
                .load(Uri.parse("file:///android_asset/" + lawyer.getUrl()))
                .centerCrop()
                .into(mAvatar);
        mPhoneNumber.setText(lawyer.getTelephone());
        mSpecialty.setText(lawyer.getClassification());
        mBio.setText(lawyer.getProduct_service());
        mUrl.setText(lawyer.getUrl());
        mMail.setText(lawyer.getMail());
    }



    private void showEditScreen() {
        Intent intent = new Intent(getActivity(), AddEditBusinessActivity.class);
        intent.putExtra(BusinessActivity.EXTRA_LAWYER_ID, mLawyerId);
        startActivityForResult(intent,BusinessFragment.REQUEST_UPDATE_DELETE_LAWYER);
    }

    private void showLawyersScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar la empresa", Toast.LENGTH_SHORT).show();
    }

    private class GetLawyerByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mLawyersDbHelper.getLawyerById(mLawyerId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showLawyer(new Business(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteLawyerTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mLawyersDbHelper.deleteBusiness(mLawyerId);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showLawyersScreen(integer > 0);
        }

    }


}
