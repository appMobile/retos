package com.trossky.developer.database.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.trossky.developer.database.model.Business;

public class LawyersDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Business.db";

    public LawyersDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase DB) {

        DB.execSQL("CREATE TABLE " + BusinessesContract.BusinessEntry.TABLE_NAME + " ("
                + BusinessesContract.BusinessEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + BusinessesContract.BusinessEntry.ID + " TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.NANE+" TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.URL+" TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.TELEPHONE+" TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.EMAIL+" TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.PRODUCT_SERVICE+" TEXT NOT NULL,"
                + BusinessesContract.BusinessEntry.CLASSIFICATION+" TEXT NOT NULL,"
                + "UNIQUE (" + BusinessesContract.BusinessEntry.ID + "))");


                ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(BusinessesContract.BusinessEntry.ID, "L-001");
        values.put(BusinessesContract.BusinessEntry.NANE, "Carlos solarte");
        values.put(BusinessesContract.BusinessEntry.URL, "HTTP//Carlos solarte");
        values.put(BusinessesContract.BusinessEntry.TELEPHONE, "010211012");
        values.put(BusinessesContract.BusinessEntry.EMAIL, "Carlos@solarte.COM");
        values.put(BusinessesContract.BusinessEntry.PRODUCT_SERVICE, "SYSWARE");
        values.put(BusinessesContract.BusinessEntry.CLASSIFICATION, "A LA MEDIDA");


        // Insertar...
        DB.insert(BusinessesContract.BusinessEntry.TABLE_NAME, null, values);

        //Otros Datos


        mockData(DB);
    }

    private void mockData(SQLiteDatabase DB){


        for (int i=0; i<10; i++){
            mockBusiness(DB,new Business("Sysware_"+i,"HTTP//Carlos solarte"+i,"010211012", "Carlos"+i+"@solarte.COM","SYSWARE"+i, "A LA MEDIDA "+i));

        }


    }
    public long mockBusiness(SQLiteDatabase DB, Business business){
        return  DB.insert(BusinessesContract.BusinessEntry.TABLE_NAME,
                null,
                business.toContentValues());

    }

    public long saveBusiness(Business business) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();//Obtener las opciones de escrucritra

        return sqLiteDatabase.insert(
                BusinessesContract.BusinessEntry.TABLE_NAME, null,business.toContentValues());
    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //Obtener lista de Empresas
    public Cursor getAllBusiness(){
        return getReadableDatabase().query(
                BusinessesContract.BusinessEntry.TABLE_NAME,null,null,null,null,null,null,null
        );
    }

    //Obterner Empresa por id
    public Cursor getLawyerById(String id_business) {
        Cursor c = getReadableDatabase().query(
                BusinessesContract.BusinessEntry.TABLE_NAME,
                null,
                BusinessesContract.BusinessEntry.ID + " LIKE ?",
                new String[]{id_business},
                null,
                null,
                null);
        return c;
    }



    public int deleteBusiness(String lawyerId) {
        return getWritableDatabase().delete(
                BusinessesContract.BusinessEntry.TABLE_NAME,
                BusinessesContract.BusinessEntry.ID + " LIKE ?",
                new String[]{lawyerId});
    }

    public int updateLawyer(Business lawyer, String lawyerId) {
        return getWritableDatabase().update(
                BusinessesContract.BusinessEntry.TABLE_NAME,
                lawyer.toContentValues(),
                BusinessesContract.BusinessEntry.ID + " LIKE ?",
                new String[]{lawyerId}
        );
    }
}
