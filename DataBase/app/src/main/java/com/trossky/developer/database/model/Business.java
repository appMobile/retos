package com.trossky.developer.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.trossky.developer.database.sqlite.BusinessesContract;

import java.util.UUID;

/**
 * Created by luis on 28/10/16.
 */

public class Business {
    private String id;
    private String name;
    private String url;
    private String telephone;
    private String mail;
    private String product_service;
    private String classification;

    public Business(String name, String url, String telephone, String mail, String product_service, String classification) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.url = url;
        this.telephone = telephone;
        this.mail = mail;
        this.product_service = product_service;
        this.classification = classification;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getProduct_service() {
        return product_service;
    }

    public void setProduct_service(String product_service) {
        this.product_service = product_service;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public ContentValues toContentValues() {

        ContentValues values= new ContentValues();
        values.put(BusinessesContract.BusinessEntry.ID,id);
        values.put(BusinessesContract.BusinessEntry.NANE,name);
        values.put(BusinessesContract.BusinessEntry.URL,url);
        values.put(BusinessesContract.BusinessEntry.TELEPHONE,telephone);
        values.put(BusinessesContract.BusinessEntry.EMAIL,mail);
        values.put(BusinessesContract.BusinessEntry.PRODUCT_SERVICE,product_service);
        values.put(BusinessesContract.BusinessEntry.CLASSIFICATION,classification);
        return values;

    }

    public Business(Cursor cursor) {
        id = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.ID));
        name = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.NANE));
        classification = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.CLASSIFICATION));
        telephone = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.TELEPHONE));
        product_service = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.PRODUCT_SERVICE));
        url = cursor.getString(cursor.getColumnIndex(BusinessesContract.BusinessEntry.URL));
    }
}

