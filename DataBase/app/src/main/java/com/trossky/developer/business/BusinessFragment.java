package com.trossky.developer.business;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.trossky.developer.addeditbusiness.AddEditBusinessActivity;
import com.trossky.developer.businessdetail.BusinessDetailActivity;
import com.trossky.developer.database.R;
import com.trossky.developer.database.sqlite.BusinessesContract;
import com.trossky.developer.database.sqlite.LawyersDbHelper;


public class BusinessFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_LAWYER = 2;

    private LawyersDbHelper mLawyersDbHelper;

    private ListView mLawyersList;
    private BusinessCursorAdapter mLawyersAdapter;
    private FloatingActionButton mAddButton;

    public BusinessFragment() {
        // Required empty public constructor
    }


    public static BusinessFragment newInstance() {
        BusinessFragment fragment = new BusinessFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_business, container, false);

        // Referencias UI
        mLawyersList = (ListView) root.findViewById(R.id.business_list);
        mLawyersAdapter = new BusinessCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mLawyersList.setAdapter(mLawyersAdapter);

        // Instancia de helper
        mLawyersDbHelper = new LawyersDbHelper(getActivity());


        // Eventos
        mLawyersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mLawyersAdapter.getItem(i);
                String currentLawyerId = currentItem.getString(
                        currentItem.getColumnIndex(BusinessesContract.BusinessEntry.ID));

                showDetailScreen(currentLawyerId);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });


        getActivity().deleteDatabase(LawyersDbHelper.DATABASE_NAME);

        // Instancia de helper
        mLawyersDbHelper = new LawyersDbHelper(getActivity());


        // Carga de datos
        loadBusiness();




        return root;
    }

    private void showAddScreen() {

        Intent intent = new Intent(getActivity(), AddEditBusinessActivity.class);
        startActivityForResult(intent, AddEditBusinessActivity.REQUEST_ADD_LAWYER);

    }

    private void showDetailScreen(String currentBusinessId) {
        Intent intent = new Intent(getActivity(), BusinessDetailActivity.class);
        intent.putExtra(BusinessActivity.EXTRA_LAWYER_ID, currentBusinessId);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_LAWYER);
    }




    private void loadBusiness() {
        new BusinessLoadTask().execute();
    }

    private class BusinessLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mLawyersDbHelper.getAllBusiness();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mLawyersAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditBusinessActivity.REQUEST_ADD_LAWYER:
                    showSuccessfullSavedMessage();
                    loadBusiness();
                    break;
                case REQUEST_UPDATE_DELETE_LAWYER:
                    loadBusiness();
                    break;
            }
        }
    }



    private void showSuccessfullSavedMessage() {

            Toast.makeText(getActivity(),
                    "Empresa guardada correctamente", Toast.LENGTH_SHORT).show();

    }

}
