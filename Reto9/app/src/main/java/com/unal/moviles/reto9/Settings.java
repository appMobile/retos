package com.unal.moviles.reto9;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by andresgutierrez on 10/29/15.
 */
public class Settings extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceValue){
        super.onCreate(savedInstanceValue);
        addPreferencesFromResource(R.xml.preferences);

        bindPreferenceSummary(findPreference(getString(R.string.distanceKey)));

    }
    private void bindPreferenceSummary(Preference preference){
        preference.setOnPreferenceChangeListener(this);
        onPreferenceChange(preference,
                PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        String value = o.toString();
        if(preference instanceof EditTextPreference){
            EditTextPreference victory = (EditTextPreference) preference;
            preference.setSummary(value);
        }
        return true;
    }
}
