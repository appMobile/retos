package com.unal.moviles.reto9;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by andresgutierrez on 10/29/15.
 */
public class SettingsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new Settings()).commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
