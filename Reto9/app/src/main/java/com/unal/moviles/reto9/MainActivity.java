package com.unal.moviles.reto9;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Marker me;
    private View root;
    private static GoogleApiClient mGoogleApiClient;
    private Location currentLocation;
    private LocationRequest request;
    private List<PlacesInformation> placesInformationList;
    private List<Marker> placesMarkers;
    private float kilometers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = View.inflate(this,R.layout.activity_main,null);
        setContentView(root);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        buildGoogleApiClient();
        placesInformationList = new ArrayList<>();
        placesMarkers = new ArrayList<>();
        loadPreferences();
    }
    private void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    private void startLocaction(){
        request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(10000);
        request.setFastestInterval(1000);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,request,this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

    }

    @Override
    public void onConnected(Bundle bundle) {
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LatLng myLocation = null;
        if(currentLocation!=null){
            myLocation = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
            BitmapDescriptor iconMarker;
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.user);
            Bitmap scaledIcon = Bitmap.createScaledBitmap(icon, 80, 80, true);
            iconMarker = BitmapDescriptorFactory.fromBitmap(scaledIcon);
            me = map.addMarker(new MarkerOptions()
                            .position(myLocation)
                            .title("Me")
                            .icon(iconMarker));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(myLocation)
                    .zoom(16)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
        StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        url.append("location="+myLocation.latitude+","+myLocation.longitude);
        url.append("&radius="+(kilometers*1000));
        url.append("&types=hospital|art_gallery|bar|bank|cafe|bus_station|museum|restaurant|university");
        url.append("&sensor=true");
        url.append("&key=AIzaSyAiM4NjcAo63RB7LwXkQBk-4lKOaNWlu0M");

        new Places().execute(url.toString());
        startLocaction();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng myLocation = new LatLng(location.getLatitude(),location.getLongitude());
        me.remove();
        BitmapDescriptor iconMarker;
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.user);
        Bitmap scaledIcon = Bitmap.createScaledBitmap(icon, 80, 80, true);
        iconMarker = BitmapDescriptorFactory.fromBitmap(scaledIcon);
        me = map.addMarker(new MarkerOptions()
                .position(myLocation)
                .title("Me")
                .icon(iconMarker));

        /*CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(myLocation)
                .zoom(16)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
        StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        url.append("location="+myLocation.latitude+","+myLocation.longitude);
        url.append("&radius="+(kilometers*1000));
        url.append("&types=hospital|art_gallery|bar|bank|cafe|bus_station|museum|restaurant|university");
        url.append("&sensor=true");
        url.append("&key=AIzaSyAiM4NjcAo63RB7LwXkQBk-4lKOaNWlu0M");

        new Places().execute(url.toString());



    }

    private class Places extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String data = strings[0];
            StringBuilder places = null;
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(data);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                inputStream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                places = new StringBuilder();
                String line="";
                while ((line=br.readLine())!=null){
                    places.append(line);
                }
                placesInformationList.clear();
                JSONObject object = new JSONObject(places.toString());
                JSONArray results = object.getJSONArray("results");
                //Log.e("algo",""+results.length());
                for(int i=0; i <results.length(); i++){
                    JSONObject geomety = results.getJSONObject(i).getJSONObject("geometry");
                    JSONObject location = geomety.getJSONObject("location");
                    double ltg = location.getDouble("lat");
                    double lgn = location.getDouble("lng");
                    String type = results.getJSONObject(i).getJSONArray("types").getString(0);
                    String name = results.getJSONObject(i).getString("name");
                    placesInformationList.add(new PlacesInformation(ltg,lgn,type,name));
                    //Log.e("algo",results.getJSONObject(i).getJSONArray("types").getString(0));

                }

            }catch (Exception e){

            }finally {

                urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(!placesMarkers.isEmpty()){
                for (Marker marker:placesMarkers){
                    marker.remove();
                }
                placesMarkers.clear();
            }
            Bitmap icon = null;
            Bitmap scaledIcon;
            BitmapDescriptor markerIcon;
            for(PlacesInformation i : placesInformationList){
                switch (i.getType()){
                    case "hospital":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.hospital);
                        break;
                    case "art_gallery":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.art_gallery);
                        break;
                    case "bar":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.bar);
                        break;
                    case "bank":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.bank);
                        break;
                    case "cafe":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.cafe);
                        break;
                    case "bus_station":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.bus);
                        break;
                    case "museum":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.museum);
                        break;
                    case "restaurant":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.food);
                        break;
                    case "university":
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.university);
                        break;

                }
                scaledIcon = Bitmap.createScaledBitmap(icon,50,50,true);
                markerIcon = BitmapDescriptorFactory.fromBitmap(scaledIcon);
                placesMarkers.add(map.addMarker(new MarkerOptions()
                    .title(i.getName())
                        .position(new LatLng(i.getLat(), i.getLng()))
                        .icon(markerIcon)));
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
    public void loadPreferences(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        kilometers = Float.parseFloat(sharedPreferences.getString(getString(R.string.distanceKey),"1"));
    }


}
