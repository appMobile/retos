package com.unal.moviles.reto9;

/**
 * Created by andresgutierrez on 10/29/15.
 */
public class PlacesInformation {
    private double lat;
    private double lng;
    private String name;
    private String type;

    public PlacesInformation(double lat, double lng, String type, String name) {
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
