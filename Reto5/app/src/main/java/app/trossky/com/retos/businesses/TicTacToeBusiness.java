package app.trossky.com.retos.businesses;



import android.os.Handler;
import android.util.Log;

import java.util.Random;

import app.trossky.com.retos.DAOs.TicTacToeDAO;
import app.trossky.com.retos.constants.K;

import static app.trossky.com.retos.constants.K.TAG_OCCUPANT;


public class TicTacToeBusiness implements TicTacToeDAO {




    private char mBoard[] = {'1','2','3','4','5','6','7','8','9'};
    public K.DifficultyLevel mDifficultyLevel= K.DifficultyLevel.Easy;
    private Handler handler;
    private Runnable runnable;



    private boolean mGameOver=false;


    public K.DifficultyLevel getmDifficultyLevel() {
        return mDifficultyLevel;
    }

    public void setmDifficultyLevel(K.DifficultyLevel mDifficultyLevel) {
        this.mDifficultyLevel = mDifficultyLevel;
    }
    public void changeDifficultyLevel(int difficulty){
        switch (difficulty){
            case 0:
                setmDifficultyLevel(K.DifficultyLevel.Easy);
                break;
            case 1:
                setmDifficultyLevel(K.DifficultyLevel.Harder);
                break;
            case 2:
                setmDifficultyLevel(K.DifficultyLevel.Expert);
                break;
            default:
                break;
        }
    }

    private Random mRand;

    public TicTacToeBusiness() {
        // Seed the random number generator
        mRand = new Random();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

            }
        };

    }



    @Override
    public void clearBoard() {
        String aux="123456789";
        mGameOver=false;
        mBoard=aux.toCharArray();
    }

    @Override
    public boolean setMove(char player, int location) {
        int move;

        move = location;
        if (move < 1 || move > K.BOARD_SIZE ||
                mBoard[move - 1] == K.HUMAN_PLAYER_1 || mBoard[move - 1] == K.HUMAN_PLAYER_2 || mBoard[move - 1] == K.COMPUTER_PLAYER) {

            if (move < 1 || move > K.BOARD_SIZE){
                System.out.println("Please enter a move between 1 and " + K.BOARD_SIZE + ".");
            }

            else{
                System.out.println("That space is occupied.  Please choose another space.");
            }

            return false;

        } else {

            mBoard[move - 1] = player;
            return true;

        }


    }


    // Check for a winner.  Return
    //  0 if no winner or tie yet
    //  1 if it's a tie
    //  2 if X won
    //  3 if O won
    @Override
    public int checkForWinner() {

        // Check horizontal wins
        for (int i = 0; i <= 6; i += 3)	{
            if (mBoard[i] == K.HUMAN_PLAYER_1 &&
                    mBoard[i+1] == K.HUMAN_PLAYER_1 &&
                    mBoard[i+2]== K.HUMAN_PLAYER_1){
                setmGameOver(true);
                return 2;
            }

            if (mBoard[i] == K.COMPUTER_PLAYER &&
                    mBoard[i+1]== K.COMPUTER_PLAYER &&
                    mBoard[i+2] == K.COMPUTER_PLAYER){
                setmGameOver(true);
                return 3;
            }

        }

        // Check vertical wins
        for (int i = 0; i <= 2; i++) {
            if (mBoard[i] == K.HUMAN_PLAYER_1 &&
                    mBoard[i + 3] == K.HUMAN_PLAYER_1 &&
                    mBoard[i + 6] == K.HUMAN_PLAYER_1) {
                setmGameOver(true);
                return 2;
            }
            if (mBoard[i] == K.COMPUTER_PLAYER &&
                    mBoard[i + 3] == K.COMPUTER_PLAYER &&
                    mBoard[i + 6] == K.COMPUTER_PLAYER){
                setmGameOver(true);
                return 3;

            }
        }

        // Check for diagonal wins
        if ((mBoard[0] == K.HUMAN_PLAYER_1 &&
                mBoard[4] == K.HUMAN_PLAYER_1 &&
                mBoard[8] == K.HUMAN_PLAYER_1) ||
                (mBoard[2] == K.HUMAN_PLAYER_1 &&
                        mBoard[4] == K.HUMAN_PLAYER_1 &&
                        mBoard[6] == K.HUMAN_PLAYER_1)){
            setmGameOver(true);
            return 2;
        }

        if ((mBoard[0] == K.COMPUTER_PLAYER &&
                mBoard[4] == K.COMPUTER_PLAYER &&
                mBoard[8] == K.COMPUTER_PLAYER) ||
                (mBoard[2] == K.COMPUTER_PLAYER &&
                        mBoard[4] == K.COMPUTER_PLAYER &&
                        mBoard[6] == K.COMPUTER_PLAYER)){
            setmGameOver(true);
            return 3;
        }

        // Check for tie
        for (int i = 0; i < K.BOARD_SIZE; i++) {
            // If we find a number, then no one has won yet
            if (mBoard[i] != K.HUMAN_PLAYER_1 && mBoard[i] != K.COMPUTER_PLAYER){
                mGameOver=false;
                return 0;
            }
        }
        mGameOver=false;

        // If we make it through the previous loop, all places are taken, so it's a tie
        return 1;
    }

    //private K.DifficultyLevel mDifficultyLevel = K.DifficultyLevel.Expert;
    @Override
    public int getComputerMove(K.DifficultyLevel mDifficultyLevel)
    {
        handler.postDelayed(runnable,1500);
        int move=-1;
        if (mDifficultyLevel.equals(K.DifficultyLevel.Easy))
            move = getRandomMove();
        else if (mDifficultyLevel.equals(K.DifficultyLevel.Harder)){
            move = getWinningMove();
            if (move==-1)
                move=getRandomMove();

        }else if (mDifficultyLevel.equals(K.DifficultyLevel.Expert)){
            move=getWinningMove();
            if (move==-1)
                move=getBlockingMove();
            if (move==-1)
                move=getRandomMove();
        }

        return move;
    }

    private int getBlockingMove() {

        // See if there's a move O can make to block X from winning
        for (int i = 0; i < K.BOARD_SIZE; i++) {
            if (mBoard[i] != K.HUMAN_PLAYER_1 && mBoard[i] != K.COMPUTER_PLAYER) {
                char curr = mBoard[i];   // Save the current number
                mBoard[i] = K.HUMAN_PLAYER_1;
                if (checkForWinner() == 2) {
                    mBoard[i] = K.COMPUTER_PLAYER;
                    System.out.println("Computer is moving to " + (i + 1));
                    mBoard[i] = curr;
                    return i;
                }
                else{
                    mBoard[i] = curr;

                }
            }
        }
        return -1;
    }

    private int getWinningMove() {
        // First see if there's a move O can make to win
        for (int i = 0; i < K.BOARD_SIZE; i++) {
            if (mBoard[i] != K.HUMAN_PLAYER_1 && mBoard[i] != K.COMPUTER_PLAYER) {
                char curr = mBoard[i];
                mBoard[i] = K.COMPUTER_PLAYER;
                if (checkForWinner() == 3) {
                    System.out.println("Computer is moving to " + (i + 1));
                    mBoard[i] = curr;
                    return i;
                }
                else
                    mBoard[i] = curr;

            }
        }
        return -1;

    }

    private int getRandomMove() {
        int move;

        // Generate random move
        do
        {
            move = mRand.nextInt(K.BOARD_SIZE);
        } while (mBoard[move] == K.HUMAN_PLAYER_1 || mBoard[move] == K.COMPUTER_PLAYER);
        return move;
    }


    public char getBoardOccupant(int i) {

        return mBoard[i];


    }
    public char[] getmBoard() {
        return mBoard;
    }

    public void setmBoard(char[] mBoard) {
        this.mBoard = mBoard;
    }

    public boolean ismGameOver() {
        return mGameOver;
    }

    public void setmGameOver(boolean mGameOver) {
        this.mGameOver = mGameOver;
    }

}
