package app.trossky.com.retos.activities;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import app.trossky.com.retos.R;
import app.trossky.com.retos.businesses.Points_GameBusiness;
import app.trossky.com.retos.businesses.TicTacToeBusiness;
import app.trossky.com.retos.constants.K;

import static app.trossky.com.retos.constants.K.HUMAN_PLAYER_1;
import static app.trossky.com.retos.constants.K.STATE_GAME;
import static app.trossky.com.retos.constants.K.TAG_TOUCH;


public class TicTacToeActivity extends AppCompatActivity {

    // Represents the internal state of the game
    private TicTacToeBusiness ticTacToeBusiness;
    private Points_GameBusiness points_gameBusiness;

    private Button newGameBtn;
    private Button mBoardButtons[];
    TextView name_player;
    TextView result_table_TextView;
    private FrameLayout frameLayoutNewGame;
    private  String result_table;
    //Player Android
    private Runnable runnable;
    private Handler handler;
    private BoardView mBoardView;
    private MediaPlayer mHumanMediaPlayer;
    private MediaPlayer mComputerMediaPlayer;
    private boolean isHumanTurn=true;
    private boolean mSoundOn=false;
    private String victoryMessage;

    //
    private SharedPreferences mPrefs;

    public String getVictoryMessage() {
        return victoryMessage;
    }

    public void setVictoryMessage(String victoryMessage) {
        this.victoryMessage = victoryMessage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        //mBoardButtons = new Button[K.BOARD_SIZE];

        mBoardView=(BoardView) findViewById(R.id.board);



        frameLayoutNewGame = (FrameLayout) findViewById(R.id.frameNnewGame);
        newGameBtn = (Button) findViewById(R.id.newGame);
        newGameBtn.setOnClickListener(new ButtonClickListener());

        name_player = (TextView) findViewById(R.id.name_player);
        result_table_TextView=(TextView) findViewById(R.id.result_table);

        ticTacToeBusiness = new TicTacToeBusiness();
        loadPreference();
        mBoardView.setmGame(ticTacToeBusiness);
        mBoardView.setOnTouchListener(mTouchListener);

        //preference
        mPrefs = getSharedPreferences("ttt_prefs", MODE_PRIVATE);

        //TODO Loading  point's players of database. Now, by default is Stored point without username
        restoreLocalData();


        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
               moveComputer();
            }
        };

        startNewGame();
        getResult_Table();
    }

    private void restoreLocalData() {

        points_gameBusiness = new Points_GameBusiness(mPrefs.getInt("mHumanWins", 0), mPrefs.getInt("mTies", 0), mPrefs.getInt("mComputerWins", 0));
        //ticTacToeBusiness.changeDifficultyLevel(mPrefs.getInt("difficulty_level", 0));

        //Restore the scores from the persistent preference data source
    }
    public void loadPreference(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int difficult = Integer.parseInt(sharedPreferences.getString(getString(R.string.difficulty_key), "1"));
        ticTacToeBusiness. changeDifficultyLevel(difficult);

        mSoundOn = sharedPreferences.getBoolean(getString(R.string.soundKey), false);
       String victoryMessage = sharedPreferences.getString(getString(R.string.victoryKey), getString(R.string.victoryDefault));
        setVictoryMessage(victoryMessage);
        //System.out.println(victoryMessage);


        mBoardView.setColor(sharedPreferences.getInt(getString(R.string.colorPickerKey), Color.BLACK));




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        switch (item.getItemId()) {
            case R.id.about_id:
                createDialogAbout(builder);
                break;
            case R.id.new_game:
                startNewGame();
                break;

            case R.id.ai_difficulty:
                Intent  intent = new Intent(this,SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.reset_score:
                resetScore(builder);
                break;
            case R.id.quit:
                System.out.println("Salir");
                createDialogExit(builder);
                return true;
            default:
                break;
        }
        return false;
    }



    private void resetScore(AlertDialog.Builder builder) {
        builder.setMessage(R.string.reset_score_question)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    public void onClick(DialogInterface  dialog, int id){
                        points_gameBusiness=new Points_GameBusiness(0,0,0);
                        getResult_Table();
                    }
                })
                .setNegativeButton(R.string.no,null);
        builder.setCancelable(false);

        builder.create().show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHumanMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.computer);
        mComputerMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.human);
    }
    @Override
    protected void onPause() {
        super.onPause();
        mHumanMediaPlayer.release();
        mComputerMediaPlayer.release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Save the current scores
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("mHumanWins", points_gameBusiness.getCount_human());
        ed.putInt("mComputerWins", points_gameBusiness.getCount_android());
        ed.putInt("mTies", points_gameBusiness.getCount_tie());
        ed.putString("victory_message",getVictoryMessage());
        ed.putInt("difficulty_level", ticTacToeBusiness.getmDifficultyLevel().ordinal());
        ed.commit();
    }

    private void createDialogAbout(AlertDialog.Builder builder) {

        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.tic_tac_toe_game_icon);
        builder.setMessage(R.string.about_info)

                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface  dialog, int id){
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(false);

        builder.create().show();
    }

        private void createDialogExit(AlertDialog.Builder builder) {

            builder.setMessage(R.string.quit_question)
                    .setCancelable(false)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                        public void onClick(DialogInterface  dialog, int id){
                            TicTacToeActivity.this.finish();
                        }
                    })
                    .setNegativeButton(R.string.no,null);
            builder.setCancelable(false);

            builder.create().show();


        }

    // Listen for touches on the board
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            // Determine which cell was touched
            int col = (int) event.getX() / mBoardView.getBoardCellWidth();
            int row = (int) event.getY() / mBoardView.getBoardCellHeight();
            int pos = row * 3 + col;

            Log.d(TAG_TOUCH," position ->"+pos);
            if (isHumanTurn && !ticTacToeBusiness.ismGameOver()){
                if(setMove(HUMAN_PLAYER_1, pos)){

                    // If no winner yet, let the human make a move
                    int winner = ticTacToeBusiness.checkForWinner();
                    if (winner == 0) {
                        isHumanTurn=false;
                        disabledBoard();
                        handler.postDelayed(runnable,1500);
                    }
                    String name=getResources().getString(R.string.turn_computer);
                    verify_winner(name,winner);
                }
            }



            return false;
        }
    };


    public void createDialogDifficult(AlertDialog.Builder builder) {

        builder.setTitle(R.string.difficulty_choose);
        final CharSequence [] levels ={
                getResources().getString(R.string.difficulty_easy),
                getResources().getString(R.string.difficulty_harder),
                getResources().getString(R.string.difficulty_expert)
        };


        int selected=ticTacToeBusiness.getmDifficultyLevel().ordinal();
        builder.setSingleChoiceItems(levels,selected,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        ticTacToeBusiness.setmDifficultyLevel(K.DifficultyLevel.Easy);
                        break;
                    case 1:
                        ticTacToeBusiness.setmDifficultyLevel(K.DifficultyLevel.Harder);
                        break;
                    case 2:
                        ticTacToeBusiness.setmDifficultyLevel(K.DifficultyLevel.Expert);
                        break;

                }



            }
        }).setNegativeButton(R.string.cancel,null);
        builder.setCancelable(false);

        builder.create().show();
    }

    // Set up the game board.
    private void startNewGame() {
        ticTacToeBusiness.clearBoard();
        mBoardView.invalidate();
        enableBoard();
        handler.removeCallbacks(runnable);
        name_player.setText( getAuth_Username()+", "+getString(R.string.first_human));
        frameLayoutNewGame.setVisibility(View.GONE);



    }

    private boolean setMove(char player, int location) {
        if (player==HUMAN_PLAYER_1 ){

        }
        if( ticTacToeBusiness.setMove(player, location + 1)){
            mBoardView.invalidate(); // Redraw the board
            if (player==HUMAN_PLAYER_1 && mSoundOn){

                mHumanMediaPlayer.start();

            }else {
                if(mSoundOn)
                    mComputerMediaPlayer.start();
            }
            return true;
        }
        return false;
    }

    public void getResult_Table(){
        result_table=getAuth_Username()+":"+ points_gameBusiness.getCount_human()+" "+
                getString(R.string.counterTie)+":"+points_gameBusiness.getCount_tie()+" "+
                getString(R.string.counterAndroid)+":"+points_gameBusiness.getCount_android();

        System.out.println(result_table);
        result_table_TextView.setText(result_table);
    }

    public void disabledBoard() {
        mBoardView.setEnabled(false);

    }
    public  void seenResult(){
        getResult_Table();
        frameLayoutNewGame.setVisibility(View.VISIBLE);

    }

    public void enableBoard() {
        mBoardView.setEnabled(true);

    }

    public String getAuth_Username() {
             /* Data user auth
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (!extras.isEmpty() || extras != null) {
            return (extras.getString("USERNAME"));

        } else {

            return "Invitado";
        }*/
        return  "Invitado";


    }





    private class ButtonClickListener implements View.OnClickListener {

        public ButtonClickListener() {
            newGameBtn.setOnClickListener(this);
        }

        public void onClick(View view) {
            if (view.getId() == R.id.newGame) {
                startNewGame();
            }
        }

    }

    private void verify_winner(String msn, int winner) {
        if (winner == 0)
            name_player.setText(msn);
        else if (winner == 1) {
            name_player.setText(R.string.result_tie);
            points_gameBusiness.sum_points(K.TIE, 1);
            disabledBoard();
            seenResult();

        } else if (winner == 2) {
            String defaultMessage = getResources().getString(R.string.result_human_wins);
            String victory_message = mPrefs.getString("victory_message", defaultMessage);


            points_gameBusiness.sum_points(HUMAN_PLAYER_1, 1);
            if (getVictoryMessage().isEmpty()){
                name_player.setText(getAuth_Username() + ", " + getString(R.string.result_human_wins));
            }else{
                name_player.setText(getAuth_Username() + ", " + getVictoryMessage());
            }


            //

            Toast.makeText(getBaseContext(),getAuth_Username() + ", " + victory_message,Toast.LENGTH_SHORT);

            disabledBoard();
            seenResult();
        } else if (winner == 3) {
            name_player.setText(R.string.result_computer_wins);
            points_gameBusiness.sum_points(K.COMPUTER_PLAYER, 1);
            disabledBoard();
            seenResult();
        }
    }

    private void moveComputer() {
        int move = ticTacToeBusiness.getComputerMove(ticTacToeBusiness.getmDifficultyLevel());
        setMove(K.COMPUTER_PLAYER, move);

        isHumanTurn=true;
        enableBoard();
        int winner = ticTacToeBusiness.checkForWinner();

        verify_winner(getAuth_Username() + ", " + getString(R.string.turn_human),winner);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i(STATE_GAME," LEVEL  ->  " + ticTacToeBusiness.getmDifficultyLevel().ordinal());

        outState.putCharArray("board", ticTacToeBusiness.getmBoard());
        outState.putBoolean("mGameOver", ticTacToeBusiness.ismGameOver());

        outState.putInt("mHumanWins", Integer.valueOf(points_gameBusiness.getCount_human()));
        outState.putInt("mComputerWins", Integer.valueOf(points_gameBusiness.getCount_android()));
        outState.putInt("mTies", Integer.valueOf(points_gameBusiness.getCount_tie()));
        outState.putBoolean("isHumanTurn", isHumanTurn);
        outState.putBoolean("mSoundOn", mSoundOn);

        outState.putInt("difficulty", ticTacToeBusiness.getmDifficultyLevel().ordinal());
        outState.putCharSequence("victory_message",getVictoryMessage());

        outState.putCharSequence("info_user", name_player.getText());
        outState.putInt("frameLayoutNewGame",frameLayoutNewGame.getVisibility());


        /*
        outState.putChar("mGoFirst", mGoFirst);*/

        handler.removeCallbacksAndMessages(null);
    }
    public static final int vis = 1;

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ticTacToeBusiness.setmBoard(savedInstanceState.getCharArray("board"));
        ticTacToeBusiness.setmGameOver(savedInstanceState.getBoolean("mGameOver"));

        points_gameBusiness.setCount_human(savedInstanceState.getInt("mHumanWins"));
        points_gameBusiness.setCount_android(savedInstanceState.getInt("mComputerWins"));
        points_gameBusiness.setCount_tie(savedInstanceState.getInt("mTies"));
        getResult_Table();
        name_player.setText(savedInstanceState.getCharSequence("info_user"));

        frameLayoutNewGame.setVisibility(savedInstanceState.getInt("frameLayoutNewGame"));
        isHumanTurn=savedInstanceState.getBoolean("isHumanTurn");
        mSoundOn=savedInstanceState.getBoolean("mSoundOn");
        setVictoryMessage(savedInstanceState.getCharSequence("victory_message").toString());
        ticTacToeBusiness.changeDifficultyLevel(savedInstanceState.getInt("difficulty"));
        if(!isHumanTurn && !ticTacToeBusiness.ismGameOver()){
            handler.postDelayed(runnable,1500);
        }else {
            mBoardView.invalidate();
        }







    }
}
