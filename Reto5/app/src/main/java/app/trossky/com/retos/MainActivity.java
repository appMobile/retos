package app.trossky.com.retos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import app.trossky.com.retos.activities.TicTacToeActivity;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {
    Button only_player;
    TextView name_player;
    EditText enter_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        only_player=(Button) findViewById(R.id.olny_player);
        enter_username= (EditText) findViewById(R.id.enter_username);
        only_player.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.olny_player:
                pressed_btnStartSession();
                break;

            default:
                break;
        }

    }
    public  void pressed_btnStartSession(){
        String msn="Inicio sesión";

        Toast.makeText(getApplicationContext(),msn,Toast.LENGTH_SHORT).show();
        Intent intent= new Intent(MainActivity.this,TicTacToeActivity.class);
        intent.putExtra("USERNAME", enter_username.getText().toString());
        startActivity(intent);

    }
}
