package app.trossky.com.retos.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

import app.trossky.com.retos.R;
import app.trossky.com.retos.businesses.TicTacToeBusiness;

import static app.trossky.com.retos.constants.K.BOARD_SIZE;
import static app.trossky.com.retos.constants.K.COMPUTER_PLAYER;
import static app.trossky.com.retos.constants.K.GRID_WIDTH;
import static app.trossky.com.retos.constants.K.HUMAN_PLAYER_1;

/**
 * Created by luis on 19/10/16.
 */

public class BoardView extends View {

    private Bitmap mHumanBitmap;
    private Bitmap mComputerBitmap;
    private Paint mPaint;
    private TicTacToeBusiness mGame;
    private int color;


    public void initialize() {
        mHumanBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.x_img);
        mComputerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.o_img);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    public BoardView(Context context, Bitmap mHumanBitmap) {
        super(context);
        initialize();
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }
    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Determine the width and height of the View
        int boardWidth = getWidth();
        int boardHeight = getHeight();
        // Make thick, light gray lines

        mPaint.setColor(color);
        mPaint.setStrokeWidth(GRID_WIDTH);


        int cellWidth = boardWidth / 3;
        int cellHeight = boardHeight / 3;

        // Draw the two vertical board lines
        canvas.drawLine(cellWidth, 0, cellWidth, boardHeight, mPaint);
        canvas.drawLine(cellWidth * 2, 0, cellWidth * 2, boardHeight, mPaint);

        // Draw the two horizontal board lines
        canvas.drawLine(0,cellHeight,boardWidth,cellHeight,mPaint);
        canvas.drawLine(0,cellHeight*2,boardWidth,cellHeight*2,mPaint);

        //Margin Board

        canvas.drawLine(0,0,getWidth(),0,mPaint);
        canvas.drawLine(0,0,0,getHeight(),mPaint);

        canvas.drawLine(getWidth(),0,getWidth(),getHeight(),mPaint);
        canvas.drawLine(0,getHeight(),getWidth(),getHeight(),mPaint);

        // Draw all the X and O images
        for (int i = 0; i < BOARD_SIZE; i++) {
            int col = i % 3;
            int row = i / 3;

            // Define the boundaries of a destination rectangle for the image
            int left = cellWidth*col;
            int top = cellHeight*row;
            int right = cellWidth*(col+1);
            int bottom = cellHeight*(row+1);

            if (mGame != null && mGame.getBoardOccupant(i) == HUMAN_PLAYER_1) {
                canvas.drawBitmap(mHumanBitmap,
                        null, // src
                        new Rect(left, top, right, bottom), // dest
                        null);
            }
            else if (mGame != null && mGame.getBoardOccupant(i) == COMPUTER_PLAYER) {
                canvas.drawBitmap(mComputerBitmap,
                        null, // src
                        new Rect(left, top, right, bottom), // dest
                        null);
            }
        }

    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setmGame(TicTacToeBusiness mGame) {
        this.mGame = mGame;
    }



    public int getBoardCellWidth() {
        return getWidth() / 3;
    }
    public int getBoardCellHeight() {
        return getHeight() / 3;
    }
}
