package app.trossky.com.retos.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class SettingsActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new Settings()).commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


}
