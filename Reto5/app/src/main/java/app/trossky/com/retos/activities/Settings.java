package app.trossky.com.retos.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import app.trossky.com.retos.R;


public class Settings extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        bindPreferenceSummary(findPreference(getString(R.string.difficulty_key)));
        //bindPreferenceSummary(findPreference(getString(R.string.VictoryKey)));


    }
    private void bindPreferenceSummary(Preference preference){
        preference.setOnPreferenceChangeListener(this);
        onPreferenceChange(preference,
                PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(),""));

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String value = newValue.toString();
        if(preference instanceof ListPreference){
            ListPreference listPreference = (ListPreference) preference;
            int preferenceIndex = listPreference.findIndexOfValue(value);
            if(preferenceIndex>=0){
                preference.setSummary(listPreference.getEntries()[preferenceIndex]);
            }
        }
        if(preference instanceof EditTextPreference){
            EditTextPreference victory = (EditTextPreference) preference;
            preference.setSummary(value);
        }
        return true;
    }
}
