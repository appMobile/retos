package agutierrezt.com.unal.reto8;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import agutierrezt.com.unal.reto8.data.Company;
import agutierrezt.com.unal.reto8.data.Product;
import agutierrezt.com.unal.reto8.data.RetoContract;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class UpdateDeleteCompany extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private View root;
    private static long id;
    private final int  COMPANY_ID = 111;
    private final int  PRODUCT_BY_COMPANY = 121;
    private Company company;
    private EditText name;
    private EditText url;
    private EditText email;
    private EditText tel;
    private Spinner type;
    private Button update;
    private static RecyclerProductsAdapter recyclerProductsAdapter;
    private static RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        root = View.inflate(this,R.layout.activity_update,null);
        super.onCreate(savedInstanceState);
        setContentView(root);
        name = (EditText) root.findViewById(R.id.company_name_update);
        url = (EditText) root.findViewById(R.id.company_url_update);
        email = (EditText) root.findViewById(R.id.company_email_update);
        tel = (EditText) root.findViewById(R.id.company_tel_update);
        type = (Spinner) root.findViewById(R.id.company_type_update);
        update = (Button) root.findViewById(R.id.company_update);
        recyclerView = (RecyclerView) root.findViewById(R.id.company_products);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerProductsAdapter = new RecyclerProductsAdapter(new ArrayList<Product>());

        recyclerView.setAdapter(recyclerProductsAdapter);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameText = name.getText().toString();
                String urlText = url.getText().toString();
                String telText = tel.getText().toString();
                String emailText = email.getText().toString();
                String typeText = String.valueOf(type.getSelectedItem());
                if(!nameText.equals("") && !urlText.equals("") && !telText.equals("")
                        && !emailText.equals("") && !typeText.equals("")){
                    ContentValues values = new ContentValues();
                    values.put(RetoContract.Company.COLUMN_NAME,nameText);
                    values.put(RetoContract.Company.COLUMN_EMAIL,emailText);
                    values.put(RetoContract.Company.COLUMN_TEL,telText);
                    values.put(RetoContract.Company.COLUMN_URL,urlText);
                    values.put(RetoContract.Company.COLUMN_TYPE, typeText);
                    getContentResolver().update(
                            RetoContract.Company.CONTENT_URI,
                            values,
                            RetoContract.Company._ID + "= ? ",
                            new String[]{""+id}
                    );
                    Intent intent = new Intent(UpdateDeleteCompany.this,MainActivity.class);
                    intent.putExtra("action",1);
                    startActivity(intent);
                }

            }
        });
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            id = extras.getLong("id");
        }
        recyclerProductsAdapter.setOnProductListener(new RecyclerProductsAdapter.OnProductListener() {
            @Override
            public void onProduct(long productId) {
                Intent intent = new Intent(UpdateDeleteCompany.this,UpdateDeleteProduct.class);
                intent.putExtra("id",id);
                intent.putExtra("productId",productId);
                startActivity(intent);
            }
        });
        if(getSupportLoaderManager().getLoader(COMPANY_ID)==null){
            getSupportLoaderManager().initLoader(COMPANY_ID,null,this);
        }else{
            getSupportLoaderManager().restartLoader(COMPANY_ID,null,this);
        }
        if(getSupportLoaderManager().getLoader(PRODUCT_BY_COMPANY)==null){
            getSupportLoaderManager().initLoader(PRODUCT_BY_COMPANY,null,this);
        }else{
            getSupportLoaderManager().restartLoader(PRODUCT_BY_COMPANY,null,this);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if( id == R.id.action_add){
            Intent intent = new Intent(UpdateDeleteCompany.this,AddProduct.class);
            intent.putExtra("id",this.id);
            startActivity(intent);
        }
        if( id == R.id.action_delete){
            getContentResolver().delete(
                    RetoContract.ProductByCompany.CONTENT_URI,
                    RetoContract.ProductByCompany.COLUMN_KEY_COMPANY + " = ?",
                    new String[]{""+this.id}
            );
            getContentResolver().delete(
                    RetoContract.Company.CONTENT_URI,
                    RetoContract.Company._ID + " = ?",
                    new String[]{""+this.id}
            );
            Intent intent = new Intent(UpdateDeleteCompany.this,MainActivity.class);
            intent.putExtra("action",2);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case COMPANY_ID:
                return new CursorLoader(
                        this,
                        RetoContract.Company.buildCompanyUri(this.id),
                        null,
                        null,
                        null,
                        null
                );
            case PRODUCT_BY_COMPANY:
                return new CursorLoader(
                        this,
                        RetoContract.ProductByCompany.buildProductByComapnyCompanyUri(this.id),
                        new String[]{
                                RetoContract.Product.TABLE_NAME+"."+ RetoContract.Product.COLUMN_NAME,
                                RetoContract.Product.TABLE_NAME+"."+ RetoContract.Product.COLUMN_DESCRIPTION,
                                RetoContract.Product.TABLE_NAME+"."+ RetoContract.Product._ID
                        },
                        null,
                        null,
                        null
                );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            switch (loader.getId()){
                case COMPANY_ID:
                    if(cursor.moveToFirst()){
                        company = new Company(
                                cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_EMAIL)),
                                cursor.getLong(cursor.getColumnIndexOrThrow(RetoContract.Company._ID)),
                                cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_NAME)),
                                cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_TEL)),
                                cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_TYPE)),
                                cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_URL))
                        );
                    }
                    name.setText(company.getName());
                    url.setText(company.getUrl());
                    email.setText(company.getEmail());
                    tel.setText(company.getTel());
                    String array[] = getResources().getStringArray(R.array.type);
                    int typePosition=0;
                    for (int i=0; i<array.length; i++){
                        if(array[i].equals(company.getType())){
                            typePosition=i;
                        }
                    }
                    type.setSelection(typePosition);

                    break;
                case PRODUCT_BY_COMPANY:
                    List<Product> products = Product.allProducts(cursor);
                    recyclerProductsAdapter.swapData(products);
                    recyclerProductsAdapter.setProductCursor(cursor);
                    break;
            }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        recyclerProductsAdapter.closeCursor();
    }
}
