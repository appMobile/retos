package agutierrezt.com.unal.reto8.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class RetoDbHelper extends SQLiteOpenHelper {
    private static  String name = "reto.db";
    private static  int version = 1;
    public static RetoDbHelper instance = null;

    public RetoDbHelper(Context context) {
        super(context, name, null, version);
    }

    public static RetoDbHelper getInstance(Context context){
        if(instance == null){
            synchronized (RetoDbHelper.class){
                if(instance == null){
                    instance = new RetoDbHelper(context);
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String company = "CREATE TABLE " + RetoContract.Company.TABLE_NAME + " ( "+
                RetoContract.Company._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                RetoContract.Company.COLUMN_NAME + " TEXT NOT NULL , "+
                RetoContract.Company.COLUMN_URL + " TEXT NOT NULL, "+
                RetoContract.Company.COLUMN_EMAIL + " TEXT NOT NULL UNIQUE ,"+
                RetoContract.Company.COLUMN_TEL + " TEXT NOT NULL ,"+
                RetoContract.Company.COLUMN_TYPE + " TEXT NOT NULL );";
        String products = "CREATE TABLE "+ RetoContract.Product.TABLE_NAME + " ( "+
                RetoContract.Product._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                RetoContract.Product.COLUMN_NAME + " TEXT NOT NULL, "+
                RetoContract.Product.COLUMN_DESCRIPTION + " TEXT NOT NULL );";
        String productsByCompany = "CREATE TABLE " + RetoContract.ProductByCompany.TABLE_NAME+ " ( "+
                RetoContract.ProductByCompany._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                RetoContract.ProductByCompany.COLUMN_KEY_COMPANY + " INTEGER NOT NULL, "+
                RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT + " INTEGER NOT NULL, "+
                "FOREIGN KEY  (" + RetoContract.ProductByCompany.COLUMN_KEY_COMPANY +" ) REFERENCES "+
                RetoContract.Company.COLUMN_NAME + " ( "+ RetoContract.Company._ID + ") ,"+
                "FOREIGN KEY  (" + RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT + " ) REFERENCES "+
                RetoContract.Product.TABLE_NAME + " ( "+ RetoContract.Product._ID + " )," +
                "UNIQUE( "+ RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT+", "+ RetoContract.ProductByCompany.COLUMN_KEY_COMPANY+" ) ON CONFLICT REPLACE );";

        sqLiteDatabase.execSQL(company);
        sqLiteDatabase.execSQL(products);
        sqLiteDatabase.execSQL(productsByCompany);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RetoContract.Company.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RetoContract.Product.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RetoContract.ProductByCompany.TABLE_NAME);

    }
}
