package agutierrezt.com.unal.reto8;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import agutierrezt.com.unal.reto8.data.Company;
import agutierrezt.com.unal.reto8.data.Product;
import agutierrezt.com.unal.reto8.data.RetoContract;

/**
 * Created by andresgutierrez on 10/22/15.
 */
public class UpdateDeleteProduct extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private View root;
    private static long id;
    private static long productId;
    private final int  PRODUCT_ID = 211;
    private EditText name;
    private EditText description;
    private Button update;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        root = View.inflate(this,R.layout.activity_update_product,null);
        super.onCreate(savedInstanceState);
        setContentView(root);
        name = (EditText) root.findViewById(R.id.product_update_name);
        description = (EditText) root.findViewById(R.id.product_update_description);

        update = (Button) root.findViewById(R.id.product_update);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            id = extras.getLong("id");
            productId = extras.getLong("productId");

        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameText = name.getText().toString();
                String descriptionText = description.getText().toString();

                if(!nameText.equals("") && !descriptionText.equals("") ){
                    ContentValues values = new ContentValues();
                    values.put(RetoContract.Product.COLUMN_NAME,nameText);
                    values.put(RetoContract.Product.COLUMN_DESCRIPTION,descriptionText);

                    getContentResolver().update(
                            RetoContract.Product.CONTENT_URI,
                            values,
                            RetoContract.Product._ID + "= ? ",
                            new String[]{""+productId}
                    );
                    Intent intent = new Intent(UpdateDeleteProduct.this,MainActivity.class);
                    intent.putExtra("action",4);
                    startActivity(intent);
                }

            }
        });


        if(getSupportLoaderManager().getLoader(PRODUCT_ID)==null){
            getSupportLoaderManager().initLoader(PRODUCT_ID,null,this);
        }else{
            getSupportLoaderManager().restartLoader(PRODUCT_ID,null,this);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_update_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if( id == R.id.action_delete){
            getContentResolver().delete(
                    RetoContract.ProductByCompany.CONTENT_URI,
                    RetoContract.ProductByCompany.COLUMN_KEY_COMPANY + " = ? AND " + RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT + " = ?",
                    new String[]{""+this.id,""+productId}
            );
            getContentResolver().delete(
                    RetoContract.Product.CONTENT_URI,
                    RetoContract.Product._ID + " = ?",
                    new String[]{""+productId}
            );
            Intent intent = new Intent(UpdateDeleteProduct.this,MainActivity.class);
            intent.putExtra("action",5);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,
                RetoContract.Product.buildProductUri(productId),
                null,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data.moveToFirst()) {
            Product product = new Product(
                    data.getString(data.getColumnIndexOrThrow(RetoContract.Product.COLUMN_DESCRIPTION)),
                    data.getLong(data.getColumnIndexOrThrow(RetoContract.Product._ID)),
                    data.getString(data.getColumnIndexOrThrow(RetoContract.Product.COLUMN_NAME))
            );
            name.setText(product.getName());
            description.setText(product.getDescription());
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
