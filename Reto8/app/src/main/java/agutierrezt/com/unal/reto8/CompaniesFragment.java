package agutierrezt.com.unal.reto8;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import agutierrezt.com.unal.reto8.data.Company;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class CompaniesFragment extends Fragment  {
    private View root;
    private static RecyclerView recyclerView;
    private static RecyclerCompaniesAdapter recyclerCompaniesAdapter;

    private static OnCompanyActivityListener onCompanyActivityListener;

    public interface OnCompanyActivityListener{
        void onCompanyActivity(long id);
    }

    public void setOnCompanyActivityListener(OnCompanyActivityListener onCompanyActivityListener) {
        this.onCompanyActivityListener = onCompanyActivityListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.show_places,container,false);
        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_companies);
        recyclerCompaniesAdapter = new RecyclerCompaniesAdapter(new ArrayList<Company>());
        recyclerCompaniesAdapter.setOnCompanyListener(new RecyclerCompaniesAdapter.OnCompanyListener() {
            @Override
            public void onCompany(long id) {
                if(onCompanyActivityListener!=null){
                    onCompanyActivityListener.onCompanyActivity(id);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerCompaniesAdapter);



        return  root;
    }

    public void swapData(List<Company> companies,Cursor data){
        recyclerCompaniesAdapter.swapData(companies);
        recyclerCompaniesAdapter.setCompaniesCursor(data);
    }
    public void closeData(){
        recyclerCompaniesAdapter.closeCursor();
    }
}
