package agutierrezt.com.unal.reto8.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.Log;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class RetoProvider extends ContentProvider {
    private static final UriMatcher uriMatcher = buildMatcher();
    private RetoDbHelper retoDbHelper;
    static final int COMPANY = 100;
    static final int COMPANY_ID = 110;
    static final int PRODUCT = 200;
    static final int PRODUCT_ID = 210;
    static final int PRODUCT_BY_COMPANY = 300;
    static final int PRODUCT_BY_COMPANY_ID = 310;
    static final int PRODUCT_BY_COMAPNY_COMPANY = 320;

    private static final SQLiteQueryBuilder productByCompanyProduct;

    static {
        productByCompanyProduct = new SQLiteQueryBuilder();
        productByCompanyProduct.setTables(
                RetoContract.Product.TABLE_NAME + " INNER JOIN " +
                        RetoContract.ProductByCompany.TABLE_NAME + " ON " +
                        RetoContract.ProductByCompany.TABLE_NAME+"."+ RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT +
                        " = " + RetoContract.Product.TABLE_NAME+"."+ RetoContract.Product   ._ID
        );
    }


    static UriMatcher buildMatcher(){
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = RetoContract.CONTENT_AUTHORITY;
        matcher.addURI(authority,RetoContract.PATH_COMPANY,COMPANY);
        matcher.addURI(authority,RetoContract.PATH_COMPANY+ "/#",COMPANY_ID);
        matcher.addURI(authority,RetoContract.PATH_PRODUCT,PRODUCT);
        matcher.addURI(authority,RetoContract.PATH_PRODUCT+"/#",PRODUCT_ID);
        matcher.addURI(authority,RetoContract.PATH_PRODUCTBYCOMPANY,PRODUCT_BY_COMPANY);
        matcher.addURI(authority,RetoContract.PATH_PRODUCTBYCOMPANY+"/#",PRODUCT_BY_COMPANY_ID);
        matcher.addURI(authority,RetoContract.PATH_PRODUCTBYCOMPANY+"/"+RetoContract.PATH_COMPANY+"/#",PRODUCT_BY_COMAPNY_COMPANY);

        return matcher;
    }

    @Override
    public boolean onCreate(){
        retoDbHelper = RetoDbHelper.getInstance(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case COMPANY:
                return RetoContract.Company.CONTENT_TYPE;
            case COMPANY_ID:
                return RetoContract.Company.CONTENT_ITEM_TYPE;
            case PRODUCT:
                return RetoContract.Product.CONTENT_TYPE;
            case PRODUCT_ID:
                return RetoContract.Product.CONTENT_ITEM_TYPE;
            case PRODUCT_BY_COMPANY:
                return RetoContract.ProductByCompany.CONTENT_TYPE;
            case PRODUCT_BY_COMPANY_ID:
                return RetoContract.ProductByCompany.CONTENT_ITEM_TYPE;
            case PRODUCT_BY_COMAPNY_COMPANY:
                return RetoContract.ProductByCompany.CONTENT_TYPE;
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor = null;
        switch (uriMatcher.match(uri)){
            case COMPANY:
                Log.v("algo","algo");
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.Company.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder


                );
                break;
            case COMPANY_ID:
                long companyId = RetoContract.Company.getCompanyId(uri);
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.Company.TABLE_NAME,
                        projection,
                        RetoContract.Company._ID + " = ?",
                        new String[]{""+companyId},
                        null,
                        null,
                        sortOrder
                );
                break;
            case PRODUCT:
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.Product.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case PRODUCT_ID:
                long productId = RetoContract.Product.getProductId(uri);
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.Product.TABLE_NAME,
                        projection,
                        RetoContract.Product._ID + "= ?",
                        new String[]{""+productId},
                        null,
                        null,
                        sortOrder
                );
                break;
            case PRODUCT_BY_COMPANY:
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.ProductByCompany.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case PRODUCT_BY_COMPANY_ID:
                long productByCompanyId = RetoContract.ProductByCompany.getProductByCompanyId(uri);
                retCursor = retoDbHelper.getReadableDatabase().query(
                        RetoContract.ProductByCompany.TABLE_NAME,
                        projection,
                        RetoContract.ProductByCompany._ID + "= ?",
                        new String[]{""+productByCompanyId},
                        null,
                        null,
                        sortOrder
                );
                break;
            case PRODUCT_BY_COMAPNY_COMPANY:

                companyId = RetoContract.ProductByCompany.getProductByCompanyCompanyId(uri);
                retCursor = productByCompanyProduct.query(
                        retoDbHelper.getReadableDatabase(),
                        projection,
                        RetoContract.ProductByCompany.COLUMN_KEY_COMPANY + " = ?",
                        new String[]{""+companyId},
                        null,
                        null,
                        null
                );
                break;
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        Uri returnUri = null;
        SQLiteDatabase  db = retoDbHelper.getWritableDatabase();
        long id;
        switch (uriMatcher.match(uri)){
            case COMPANY:
                id = db.insert(RetoContract.Company.TABLE_NAME, null, contentValues);
                if(id>0){
                    returnUri = RetoContract.Company.buildCompanyUri(id);
                }else{
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            case PRODUCT:
                id = db.insert(RetoContract.Product.TABLE_NAME, null, contentValues);
                if(id>0){
                    returnUri = RetoContract.Product.buildProductUri(id);
                }else{
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            case PRODUCT_BY_COMPANY:
                id = db.insert(RetoContract.ProductByCompany.TABLE_NAME, null, contentValues);
                if(id>0){
                    returnUri = RetoContract.ProductByCompany.buildProductByCompanyUri(id);
                }else{
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }

        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted=0;
        final SQLiteDatabase db = retoDbHelper.getWritableDatabase();
        if ( null == selection ) selection = "1";
        switch (uriMatcher.match(uri)){
            case COMPANY:
                rowsDeleted = db.delete(RetoContract.Company.TABLE_NAME,selection,selectionArgs);
                break;
            case PRODUCT:
                rowsDeleted = db.delete(RetoContract.Product.TABLE_NAME,selection,selectionArgs);
                break;
            case PRODUCT_BY_COMPANY:
                rowsDeleted = db.delete(RetoContract.ProductByCompany.TABLE_NAME,selection,selectionArgs);
                break;
        }
        if(rowsDeleted!=0){
            getContext().getContentResolver().notifyChange(uri,null);
        }
        return rowsDeleted;
    }



    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = retoDbHelper.getWritableDatabase();
        int rowUpdated = 0;
        switch (uriMatcher.match(uri)){
            case COMPANY:
                rowUpdated = db.update(RetoContract.Company.TABLE_NAME,contentValues,selection,selectionArgs);
                break;
            case PRODUCT:
                rowUpdated = db.update(RetoContract.Product.TABLE_NAME,contentValues,selection,selectionArgs);
                break;
            case PRODUCT_BY_COMPANY:
                rowUpdated = db.update(RetoContract.ProductByCompany.TABLE_NAME,contentValues,selection,selectionArgs);
                break;
        }
        if(rowUpdated!=0){
            getContext().getContentResolver().notifyChange(uri,null);
        }

        return rowUpdated;
    }


}
