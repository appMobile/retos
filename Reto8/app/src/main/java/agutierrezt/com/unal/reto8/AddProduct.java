package agutierrezt.com.unal.reto8;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import agutierrezt.com.unal.reto8.data.Product;
import agutierrezt.com.unal.reto8.data.RetoContract;

/**
 * Created by andresgutierrez on 10/22/15.
 */
public class AddProduct extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private EditText name;
    private EditText description;
    private Button add;
    private View root;
    private static long id;
    private static RecyclerView recyclerView;
    private static RecyclerProductsAdapter recyclerProductsAdapter;
    private static int PRODUCTS = 512;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        root = View.inflate(this,R.layout.activity_add_product,null);
        super.onCreate(savedInstanceState);
        setContentView(root);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            id = extras.getLong("id");
        }
        recyclerView = (RecyclerView) root.findViewById(R.id.add_products);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerProductsAdapter = new RecyclerProductsAdapter(new ArrayList<Product>());
        recyclerProductsAdapter.setOnProductListener(new RecyclerProductsAdapter.OnProductListener() {
            @Override
            public void onProduct(long productId) {
                ContentValues values = new ContentValues();
                values.put(RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT, productId);
                values.put(RetoContract.ProductByCompany.COLUMN_KEY_COMPANY, id);
                getContentResolver().insert(
                        RetoContract.ProductByCompany.CONTENT_URI,
                        values
                );
                Intent intent = new Intent(AddProduct.this, MainActivity.class);
                intent.putExtra("action", 3);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(recyclerProductsAdapter);
        name = (EditText) root.findViewById(R.id.product_add_name);
        description = (EditText) root.findViewById(R.id.product_add_description);
        add = (Button) root.findViewById(R.id.product_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameText = name.getText().toString();
                String descriptionText = description.getText().toString();
                if(!nameText.equals("") && !descriptionText.equals("")){
                    ContentValues values =new ContentValues();
                    values.put(RetoContract.Product.COLUMN_NAME,nameText);
                    values.put(RetoContract.Product.COLUMN_DESCRIPTION,descriptionText);
                    Uri uri = getContentResolver().insert(
                            RetoContract.Product.CONTENT_URI,
                            values
                    );
                    long idProduct = RetoContract.Product.getProductId(uri);
                    ContentValues otherValues = new ContentValues();
                    otherValues.put(RetoContract.ProductByCompany.COLUMN_KEY_COMPANY,id);
                    otherValues.put(RetoContract.ProductByCompany.COLUMN_KEY_PRODUCT, idProduct);
                    getContentResolver().insert(
                            RetoContract.ProductByCompany.CONTENT_URI,
                            otherValues
                    );
                    Intent intent = new Intent(AddProduct.this,MainActivity.class);
                    intent.putExtra("action",3);
                    startActivity(intent);
                }
            }
        });
        if(getSupportLoaderManager().getLoader(PRODUCTS)==null){
            getSupportLoaderManager().initLoader(PRODUCTS,null,this);
        }else{
            getSupportLoaderManager().restartLoader(PRODUCTS,null,this);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        name.setText(savedInstanceState.getString("name"));
        name.setText(savedInstanceState.getString("description"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("name",name.getText().toString());
        outState.putString("description",description.getText().toString());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,
                RetoContract.Product.CONTENT_URI,
                null,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        List<Product> products = Product.allProducts(data);
        recyclerProductsAdapter.swapData(products);
        recyclerProductsAdapter.setProductCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        recyclerProductsAdapter.closeCursor();
    }
}
