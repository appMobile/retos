package agutierrezt.com.unal.reto8;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import agutierrezt.com.unal.reto8.data.Company;
import agutierrezt.com.unal.reto8.data.RetoContract;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    //TODO
    /*
    I lack to implement the searchview and others different of queries also I need to create a new view for the creation of products
    and their respective CRUD
     */
    private View root;
    private CompaniesFragment companiesFragment;
    private SearchView searchView;
    private String name;
    private String query = null;
    private String queryArgs[] ;
    private List<String> args;
    private static int COMPANIES = 100;
    private CheckBox cons;
    private CheckBox perf;
    private CheckBox com;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        root = View.inflate(this,R.layout.activity_main,null);
        super.onCreate(savedInstanceState);
        setContentView(root);
        args = new ArrayList<>();
        companiesFragment = new CompaniesFragment();
        companiesFragment.setOnCompanyActivityListener(new CompaniesFragment.OnCompanyActivityListener() {
            @Override
            public void onCompanyActivity(long id) {
                Intent intent = new Intent(MainActivity.this, UpdateDeleteCompany.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        cons = (CheckBox) root.findViewById(R.id.cons);
        perf = (CheckBox) root.findViewById(R.id.perf);
        com = (CheckBox) root.findViewById(R.id.com);
        cons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleIntent(getIntent());
            }
        });
        perf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleIntent(getIntent());
            }
        });
        com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleIntent(getIntent());
            }
        });
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            int id = extras.getInt("action");
            if(id == 1){
                Toast.makeText(this,"we update a record",Toast.LENGTH_SHORT).show();
            }else if(id ==2){
                Toast.makeText(this,"we delete a record",Toast.LENGTH_SHORT).show();
            }else if(id ==3){
                Toast.makeText(this,"we ada a product",Toast.LENGTH_SHORT).show();
            } else if(id ==4){
                Toast.makeText(this,"we update a product",Toast.LENGTH_SHORT).show();
            }else if(id ==5){
                Toast.makeText(this,"we delete a product",Toast.LENGTH_SHORT).show();
            }
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.show_companies_container, companiesFragment)
                .commit();
        if(getSupportLoaderManager().getLoader(COMPANIES)==null){
            getSupportLoaderManager().initLoader(COMPANIES,null,this);
        }else{
            getSupportLoaderManager().restartLoader(COMPANIES,null,this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if( id == R.id.action_add){
            Intent intent = new Intent(MainActivity.this,AddCompany.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    public void handleIntent(Intent intent){
        args = new ArrayList<>();
        query = "";
        if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
            name = intent.getStringExtra(SearchManager.QUERY);

        }
        if(name!=null && name.toLowerCase().equals("all places")){
            query = null;
            queryArgs = null;
        }
        else if(name!=null) {
            args.add("%" + name + "%");
            query = RetoContract.Company.COLUMN_NAME + " LIKE ?";
            //queryArgs = new String[args.size()];
            //args.toArray(queryArgs);
        }
        List<Boolean> checks = new ArrayList<>();
        String [] arrays = getResources().getStringArray(R.array.type);
        checks.add(cons.isChecked());
        checks.add(perf.isChecked());
        checks.add(com.isChecked());
        String temp = RetoContract.Company.COLUMN_TYPE + " IN ( ";
        boolean isChecked = false;
        for(int i=0; i<checks.size(); i++){
            if(checks.get(i)){
                args.add(arrays[i]);
                temp+= " ?,";
                isChecked = true;
            }
        }
        temp = temp.substring(0,temp.length()-1);
        temp += " )";
        if(isChecked){
            if(query!=null && !query.equals("")){
                query += " AND " + temp;
            }else{
                query = temp;
            }
            queryArgs = new String[args.size()];
            args.toArray(queryArgs);
        }else {
            if(query!=null && !query.equals("")){
                queryArgs = new String[args.size()];
                args.toArray(queryArgs);
            }else{
                queryArgs = null;
                query = null;
            }
        }


        if(getSupportLoaderManager().getLoader(COMPANIES)==null){
            getSupportLoaderManager().initLoader(COMPANIES,null,this);
        }else{
            getSupportLoaderManager().restartLoader(COMPANIES,null,this);
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,
                RetoContract.Company.CONTENT_URI,
                null,
                query,
                queryArgs,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        List<Company> companies = Company.allCompanies(data);
        companiesFragment.swapData(companies,data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        companiesFragment.closeData();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("cons", cons.isChecked());
        outState.putBoolean("perf", perf.isChecked());
        outState.putBoolean("com",com.isChecked());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cons.setSelected(savedInstanceState.getBoolean("cons"));
        perf.setSelected(savedInstanceState.getBoolean("perf"));
        com.setSelected(savedInstanceState.getBoolean("com"));
        handleIntent(getIntent());
    }
}
