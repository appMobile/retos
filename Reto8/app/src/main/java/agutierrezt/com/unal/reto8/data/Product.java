package agutierrezt.com.unal.reto8.data;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class Product {
    private String name;
    private String description;
    private long id;

    public Product(String description, long id, String name) {
        this.description = description;
        this.id = id;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Product> allProducts(Cursor data){
        List<Product> products = new ArrayList<>();
        while (data.moveToNext()){
            products.add(
                    new Product(
                            data.getString(data.getColumnIndexOrThrow(RetoContract.Product.COLUMN_DESCRIPTION)),
                            data.getLong(data.getColumnIndexOrThrow(RetoContract.Product._ID)),
                            data.getString(data.getColumnIndexOrThrow(RetoContract.Product.COLUMN_NAME))
                    )
            );
        }
        return products;
    }
}
