package agutierrezt.com.unal.reto8;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import agutierrezt.com.unal.reto8.data.Product;

/**
 * Created by andresgutierrez on 10/22/15.
 */
public class RecyclerProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Cursor productCursor;
    private static List<Product> products;
    public static  OnProductListener onProductListener;

    public interface OnProductListener{
        void onProduct(long productId);
    }

    public static void setOnProductListener(OnProductListener onProductListener) {
        RecyclerProductsAdapter.onProductListener = onProductListener;
    }

    public RecyclerProductsAdapter(List<Product> products) {
        this.products = products;
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;
        private TextView description;
        public ProductViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.product_name);
            description = (TextView) itemView.findViewById(R.id.product_description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onProductListener!=null){
                Product product = products.get(getAdapterPosition());
                onProductListener.onProduct(product.getId());
            }
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_product,parent,false);
        viewHolder = new ProductViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductViewHolder productViewHolder = (ProductViewHolder) holder;
        Product product = products.get(position);
        productViewHolder.name.setText("name: "+product.getName());
        productViewHolder.description.setText("descripton: "+product.getDescription());
    }

    public void swapData(List<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }
    public void setProductCursor(Cursor cursor){
        if(productCursor !=null){
            closeCursor();
        }
        this.productCursor = cursor;
    }
    public void closeCursor(){
        if(productCursor!=null) {
            productCursor.close();
            productCursor = null;
        }

    }
}
