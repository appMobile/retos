package agutierrezt.com.unal.reto8;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import agutierrezt.com.unal.reto8.data.Company;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class RecyclerCompaniesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Cursor companyCursor;
    private static List<Company> companies;
    private static OnCompanyListener onCompanyListener;

    public interface OnCompanyListener{
        void onCompany(long id);
    }


    public static void setOnCompanyListener(OnCompanyListener onCompanyListener) {
        RecyclerCompaniesAdapter.onCompanyListener = onCompanyListener;
    }

    public static class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;
        private TextView url;
        public CompanyViewHolder(View itemView){
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.company_name);
            url = (TextView) itemView.findViewById(R.id.company_url);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if(onCompanyListener!=null){
                onCompanyListener.onCompany(companies.get(getAdapterPosition()).getId());
            }
        }
    }

    public RecyclerCompaniesAdapter(List<Company> companies){
        this.companies = companies;
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_companie, viewGroup, false);
        CompanyViewHolder companyViewHolder = new CompanyViewHolder(view);
        return companyViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CompanyViewHolder companyViewHolder = (CompanyViewHolder) holder;
        Company company = companies.get(position);
        companyViewHolder.name.setText("Name: "+company.getName());
        companyViewHolder.url.setText("Url: "+company.getUrl());
    }

    public void swapData(List<Company> companies){
        this.companies = companies;
        notifyDataSetChanged();
    }
    public void setCompaniesCursor(Cursor cursor){
        if(companyCursor !=null){
            closeCursor();
        }
        this.companyCursor = cursor;
    }
    public void closeCursor(){
        if(companyCursor!=null) {
            companyCursor.close();
            companyCursor = null;
        }

    }
}
