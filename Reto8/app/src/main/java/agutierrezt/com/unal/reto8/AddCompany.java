package agutierrezt.com.unal.reto8;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import agutierrezt.com.unal.reto8.data.RetoContract;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class AddCompany extends AppCompatActivity {

    private View root;
    private EditText name;
    private EditText url;
    private EditText tel;
    private EditText email;
    private Spinner type;
    private Button add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        root = View.inflate(this,R.layout.activity_add,null);
        super.onCreate(savedInstanceState);
        setContentView(root);
        name = (EditText) root.findViewById(R.id.company_name_add);
        url = (EditText) root.findViewById(R.id.company_url_add);
        tel = (EditText) root.findViewById(R.id.company_tel_add);
        email = (EditText) root.findViewById(R.id.company_email_add);
        type = (Spinner) root.findViewById(R.id.company_type_add);
        add = (Button) root.findViewById(R.id.company_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameText = name.getText().toString();
                String urlText = url.getText().toString();
                String telText = tel.getText().toString();
                String emailText = email.getText().toString();
                String typeText = String.valueOf(type.getSelectedItem());
                if(!nameText.equals("") && !urlText.equals("") && !telText.equals("")
                        && !emailText.equals("") && !typeText.equals("")){
                    ContentValues values = new ContentValues();
                    values.put(RetoContract.Company.COLUMN_NAME,nameText);
                    values.put(RetoContract.Company.COLUMN_EMAIL,emailText);
                    values.put(RetoContract.Company.COLUMN_TEL,telText);
                    values.put(RetoContract.Company.COLUMN_URL,urlText);
                    values.put(RetoContract.Company.COLUMN_TYPE, typeText);
                    getContentResolver().insert(
                            RetoContract.Company.CONTENT_URI,
                            values
                    );
                    Intent intent = new Intent(AddCompany.this,MainActivity.class);
                    startActivity(intent);
                }
            }
        });


    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        name.setText(savedInstanceState.getString("name"));
        url.setText(savedInstanceState.getString("url"));
        email.setText(savedInstanceState.getString("email"));
        tel.setText(savedInstanceState.getString("tel"));
        type.setSelection(savedInstanceState.getInt("type"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("name", name.getText().toString());
        outState.putString("url", url.getText().toString());
        outState.putString("email",email.getText().toString());
        outState.putString("tel",tel.getText().toString());
        outState.putInt("type",type.getSelectedItemPosition());
    }

}
