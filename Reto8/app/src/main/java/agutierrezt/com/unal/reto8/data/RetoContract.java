package agutierrezt.com.unal.reto8.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class RetoContract {

    public static final String CONTENT_AUTHORITY = "agutierrezt.com.unal.reto8";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_COMPANY = "company";
    public static final String PATH_PRODUCT = "product";
    public static final String PATH_PRODUCTBYCOMPANY = "productByCompany";

    public static class Company implements BaseColumns{

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_COMPANY).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMPANY;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMPANY;

        public static Uri buildCompanyUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
        public static long getCompanyId(Uri uri){
            return Long.parseLong(uri.getLastPathSegment());
        }

        public static final String TABLE_NAME = "Company";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_URL = "url";
        public static final String COLUMN_TEL = "telephone";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_TYPE = "type";

    }
    public static class Product implements BaseColumns {

        public static final Uri CONTENT_URI =  BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCT).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCT;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" +PATH_PRODUCT;
        public static final String TABLE_NAME = "ProductsAndServices";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";

        public static Uri buildProductUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
        public static long getProductId(Uri uri){
            return Long.parseLong(uri.getLastPathSegment());
        }

    }
    public static class ProductByCompany implements BaseColumns{

        public static final Uri CONTENT_URI =  BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCTBYCOMPANY).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTBYCOMPANY;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" +PATH_PRODUCTBYCOMPANY;
        public static final String TABLE_NAME = "ProductsByCompany";
        public static final String COLUMN_KEY_COMPANY = "company_id";
        public static final String COLUMN_KEY_PRODUCT = "product_id";

        public static Uri buildProductByCompanyUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
        public static long getProductByCompanyId(Uri uri){
            return Long.parseLong(uri.getLastPathSegment());
        }
        public static Uri buildProductByComapnyCompanyUri(long id){
            return CONTENT_URI.buildUpon().appendPath(PATH_COMPANY)
                    .appendPath(""+id).build();
        }
        public static Long getProductByCompanyCompanyId(Uri uri){
            return Long.parseLong(uri.getPathSegments().get(2));
        }
    }
}
