package agutierrezt.com.unal.reto8.data;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresgutierrez on 10/21/15.
 */
public class Company {
    private String name;
    private long id;
    private String email;
    private String url;
    private String type;
    private String tel;

    public Company(String email, long id, String name, String tel, String type, String url) {
        this.email = email;
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.type = type;
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public static List<Company> allCompanies(Cursor cursor){
        List<Company> companies = new ArrayList<>();
        while (cursor.moveToNext()){
            companies.add(
                    new Company(
                            cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_EMAIL)),
                            cursor.getLong(cursor.getColumnIndexOrThrow(RetoContract.Company._ID)),
                            cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_TEL)),
                            cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_TYPE)),
                            cursor.getString(cursor.getColumnIndexOrThrow(RetoContract.Company.COLUMN_URL))
                    )
            );
        }
        return companies;
    }
}
