package com.trossky.dev.reto4.constants;

import android.graphics.Color;

/**
 * Created by luis on 20/08/16.
 */
public class K {

    public static  final int BOARD_SIZE = 9;
    // Characters used to represent the human, computer, and open spots
    public static final char OPEN_SPOT = ' ';
    public static final char HUMAN_PLAYER_1 = 'X';
    public static final char HUMAN_PLAYER_2 = 'Y';
    public static final char COMPUTER_PLAYER = 'O';

    public static final int DIALOG_DIFFICULTY_ID = 0;
    public static final int DIALOG_QUIT_ID = 1;

    //Character of TIE
    public static final char TIE = 'T';

    public static  final  int COLOR_HUMAN_PLAYER=Color.rgb(0, 200, 0);
    public static  final  int COLOR_COMPUTER_PLAYER=Color.rgb(200, 0, 0);
    public static final int ABOUT_ID = 3;

    // The computer's difficulty levels

    public enum DifficultyLevel {Easy, Harder, Expert};


}
