package com.trossky.dev.reto4.activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.trossky.dev.reto4.MainActivity;
import com.trossky.dev.reto4.R;
import com.trossky.dev.reto4.businesses.Points_GameBusiness;
import com.trossky.dev.reto4.businesses.TicTacToeBusiness;
import com.trossky.dev.reto4.constants.K;


public class TicTacToeActivity extends AppCompatActivity {


    // Represents the internal state of the game
    private TicTacToeBusiness ticTacToeBusiness;
    private Points_GameBusiness points_gameBusiness;
    public K.DifficultyLevel mDifficultyLevel=K.DifficultyLevel.Harder;
    public MainActivity mainActivity;


    private Button newGameBtn;
    private Button mBoardButtons[];
    TextView name_player;
    TextView result_table_TextView;
    private FrameLayout frameLayoutNewGame;
    private  String result_table;
    final Context context = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        mBoardButtons = new Button[K.BOARD_SIZE];

        mBoardButtons[0] = ((Button) findViewById(R.id.b1));
        mBoardButtons[1] = ((Button) findViewById(R.id.b2));
        mBoardButtons[2] = ((Button) findViewById(R.id.b3));
        mBoardButtons[3] = ((Button) findViewById(R.id.b4));
        mBoardButtons[4] = ((Button) findViewById(R.id.b5));
        mBoardButtons[5] = ((Button) findViewById(R.id.b6));
        mBoardButtons[6] = ((Button) findViewById(R.id.b7));
        mBoardButtons[7] = ((Button) findViewById(R.id.b8));
        mBoardButtons[8] = ((Button) findViewById(R.id.b9));
        frameLayoutNewGame = (FrameLayout) findViewById(R.id.frameNnewGame);
        newGameBtn = (Button) findViewById(R.id.newGame);
        name_player = (TextView) findViewById(R.id.name_player);
        result_table_TextView=(TextView) findViewById(R.id.result_table);

        ticTacToeBusiness = new TicTacToeBusiness();


        //TODO Loading  point's players of database. Now, by default is 0,0,0
        points_gameBusiness = new Points_GameBusiness(0, 0, 0);




        startNewGame();
        getResult_Table();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.about_id:
                showDialog(K.ABOUT_ID);
                break;
            case R.id.new_game:
                startNewGame();
                break;
            case R.id.ai_difficulty:

                showDialog(K.DIALOG_DIFFICULTY_ID);

                return true;
            case R.id.quit:
                System.out.println("Salir");


                showDialog(K.DIALOG_QUIT_ID);



                return true;
        }
        return false;
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog= null;

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        int selected;
        switch (id){
            case K.DIALOG_DIFFICULTY_ID:



                builder.setTitle(R.string.difficulty_choose);

                final CharSequence [] levels ={
                        getResources().getString(R.string.difficulty_easy),
                        getResources().getString(R.string.difficulty_harder),
                        getResources().getString(R.string.difficulty_expert)
                };

                selected=mDifficultyLevel.ordinal();
                builder.setSingleChoiceItems(levels, selected,

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item){
                                dialog.dismiss();
                                switch (item) {
                                    case 0:
                                        setmDifficultyLevel(K.DifficultyLevel.Easy);
                                        break;
                                    case 1:
                                        setmDifficultyLevel(K.DifficultyLevel.Harder);


                                        break;

                                    case 2:
                                        setmDifficultyLevel(K.DifficultyLevel.Expert);


                                        break;
                                }

                            }


                        });




                dialog=builder.create();


                break;
            case K.DIALOG_QUIT_ID:
                builder.setMessage(R.string.quit_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                            public void onClick(DialogInterface  dialog, int id){
                                TicTacToeActivity.this.finish();



                            }
                        })
                        .setNegativeButton(R.string.no,null);
                dialog=builder.create();
                break;
            case K.ABOUT_ID:
                builder.setTitle(R.string.app_name);
                builder.setIcon(R.mipmap.tic_tac_toe_game_icon);
                builder.setMessage(R.string.about_info)

                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface  dialog, int id){
                                dialog.dismiss();

                            }
                        });
                dialog=builder.create();
                break;
        }
        return dialog;


    }





    // Set up the game board.
    private void startNewGame() {
        ticTacToeBusiness.clearBoard();
        for (int i = 0; i < mBoardButtons.length; i++) {
            mBoardButtons[i].setText(" ");
            mBoardButtons[i].setEnabled(true);
            mBoardButtons[i].setOnClickListener(new ButtonClickListener(i));
        }

        name_player.setText( getAuth_Username()+", "+getString(R.string.first_human));
        frameLayoutNewGame.setVisibility(View.GONE);



    }

    private void setMove(char player, int location) {
        ticTacToeBusiness.setMove(player, location + 1);
        mBoardButtons[location].setEnabled(false);
        mBoardButtons[location].setText(String.valueOf(player));
        if (player == K.HUMAN_PLAYER_1)
            mBoardButtons[location].setTextColor(K.COLOR_HUMAN_PLAYER);
        else
            mBoardButtons[location].setTextColor(K.COLOR_COMPUTER_PLAYER);
    }
    public void getResult_Table(){
        result_table=getAuth_Username()+":"+ points_gameBusiness.getCount_human()+" "+
                getString(R.string.counterTie)+":"+points_gameBusiness.getCount_tie()+" "+
                getString(R.string.counterAndroid)+":"+points_gameBusiness.getCount_android();

        System.out.println(result_table);
        result_table_TextView.setText(result_table);
    }

    public void disabledBoard() {
        System.out.println("New" +
                "");
        getResult_Table();



        for (int i = 0; i < mBoardButtons.length; i++) {
            mBoardButtons[i].setEnabled(false);
        }
        frameLayoutNewGame.setVisibility(View.VISIBLE);
    }

    public String getAuth_Username() {
             /* Data user auth*/
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (!extras.isEmpty() || extras != null) {
            return (extras.getString("USERNAME"));

        } else {

            return "Invitado";
        }


    }




    public K.DifficultyLevel getmDifficultyLevel() {
        return mDifficultyLevel;
    }

    public void setmDifficultyLevel(K.DifficultyLevel mDifficultyLevel) {
        this.mDifficultyLevel = mDifficultyLevel;
    }

    private class ButtonClickListener implements View.OnClickListener {

        int location;



        public ButtonClickListener(int location) {

            this.location = location;
            newGameBtn.setOnClickListener(this);
        }

        public void onClick(View view) {

            if (mBoardButtons[location].isEnabled()) {
                setMove(K.HUMAN_PLAYER_1, location);
                // If no winner yet, let the computer make a move
                int winner = ticTacToeBusiness.checkForWinner();
                if (winner == 0) {
                    name_player.setText(R.string.turn_computer);
                    int move = ticTacToeBusiness.getComputerMove(mDifficultyLevel);
                    setMove(K.COMPUTER_PLAYER, move);
                    winner = ticTacToeBusiness.checkForWinner();
                }

                if (winner == 0)
                    name_player.setText(getAuth_Username() + ", " + getString(R.string.turn_human));
                else if (winner == 1) {
                    name_player.setText(R.string.result_tie);
                    points_gameBusiness.sum_points(K.TIE, 1);

                    disabledBoard();

                } else if (winner == 2) {
                    name_player.setText(getAuth_Username() + ", " + getString(R.string.result_human_wins));
                    points_gameBusiness.sum_points(K.HUMAN_PLAYER_1, 1);
                    disabledBoard();
                } else {
                    name_player.setText(R.string.result_computer_wins);
                    points_gameBusiness.sum_points(K.COMPUTER_PLAYER, 1);
                    disabledBoard();

                }

            }
            if (view.getId() == R.id.newGame) {
                startNewGame();
            }


        }

    }
}
