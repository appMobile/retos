package com.trossky.dev.reto4.businesses;

import com.trossky.dev.reto4.constants.K;

public class Points_GameBusiness {



    int count_human;
    int count_tie;
    int count_android;

    public Points_GameBusiness(int count_human, int count_tie, int count_android) {
        this.count_human = count_human;
        this.count_tie = count_tie;
        this.count_android = count_android;
    }

    public  void  sum_points(char name_player, int id_player){
        int aux;
        switch (name_player){
            case K.HUMAN_PLAYER_1:
                setCount_human(getCount_human()+1);
                break;
            case K.COMPUTER_PLAYER:
                setCount_android(getCount_android()+1);
                break;
            case K.TIE:
                setCount_tie(getCount_tie()+1);
                break;
            default:
                break;
        }

    }

    public int getCount_human() {
        return count_human;
    }

    public void setCount_human(int count_human) {
        this.count_human = count_human;
    }

    public int getCount_tie() {
        return count_tie;
    }

    public void setCount_tie(int count_tie) {
        this.count_tie = count_tie;
    }

    public int getCount_android() {
        return count_android;
    }

    public void setCount_android(int count_android) {
        this.count_android = count_android;
    }

    public Points_GameBusiness resul(){
        return  new Points_GameBusiness(getCount_human(),getCount_android(),getCount_tie());
    }
}
